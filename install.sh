sudo apt-get update
sudo apt-get install mongodb-server -y
sudo systemctl enable mongodb
sudo systemctl start mongodb

wget https://nodejs.org/dist/v8.9.4/node-v8.9.4-linux-armv7l.tar.gz
tar -xvzf node-v8.9.4-linux-armv7l.tar.gz
sudo mv node-v8.9.4-linux-armv7l /opt/node
rm node-v8.9.4-linux-armv7l.tar.gz
sudo ln -s /opt/node/bin/* /usr/bin/

sudo apt-get install libbluetooth-dev -y
sudo apt-get install libudev-dev -y

sudo npm i --unsafe-perm

sudo rm -f /etc/systemd/system/autofood.service

sudo echo "
[Unit]
Description=autofood
Requires=mongodb.service
After=mongodb.service

[Service]
WorkingDirectory=/home/pi/autofood
ExecStart=/bin/sh /home/pi/autofood/updateAndRun.sh
TimeoutStartSec=0
Restart=on-failure
StartLimitIntervalSec=60
StartLimitBurst=3

[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/autofood.service

sudo systemctl enable autofood

git remote add gitlab "https://oauth2:xFSmxtD4b9y2nZADvCEf@gitlab.com/morpheus5555/autofood-outdoor.git"