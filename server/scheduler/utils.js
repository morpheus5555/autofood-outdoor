function getToday() {
    return Date(Date.now()).split(' ') 	// Today timestamp to array
	    .slice(0,4).join(' '); 					// Keep [Month, Day, Hours and Year] only
}

function todayAt(hours,minutes=0,seconds=0) {
    const today = getToday()
    return Date.parse(`${today} ${hours}:${minutes}:${seconds}`); // Add Custom Hour and Parse it
}

function inSecond(seconds) {
    return Date.now() + (seconds * 1000)

}

module.exports = {
    getToday,
    todayAt,
    inSecond
}