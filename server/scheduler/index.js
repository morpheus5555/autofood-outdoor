const aO = require('../actions/actionObjects').actions
const ScheduleItem = require('./item')
const SchedulerItemModel = require('./item_model').model
const Scheduler = require('mongoose').model('Scheduler');
const BotModel = require('mongoose').model('Bot')
const botExist = require('../createBot').botExist
const events = require('events')
const utils = require('./utils')

let items = []
let history = []

const add = (schedule_item) => {
    if(is.error(validate(schedule_item))) return;

    // Stock in RAM
    items.push(schedule_item)

    // Create in database
    SchedulerItemModel.create(schedule_item)
        .then(Scheduler.addItem)
        .catch(log.onerr)
}

const addMultiple = (schedule_items) => {
    schedule_items.map(_item => {
        add(_item)
    })
}

const validate = (schedule_item) => {
    if(schedule_item.time_trigger < Date.now())
        return log.err('You cannot add a schedule item in the past');
}

const fill = (scheduler) => {
    if(scheduler === null) {
        log.err("Cannot fill because 'scheduler' is null")
        return;   
    }

    if(scheduler.items.length === 0) {
        addMultiple([
            new ScheduleItem("a", aO.move('y', 300), utils.inSecond(60), 5000),
            new ScheduleItem("b", aO.move('y', 4000), utils.inSecond(80), 10000),
        ])
    }

    history = scheduler.history || []

    return SchedulerItemModel
        .getToday()
        .then((_items) => {
            items = _items
        })
} 

const init = function() {
    return Scheduler.get()
        .then(fill)
}

module.exports = {
    init,
    items,
    add,
    addMultiple,
}