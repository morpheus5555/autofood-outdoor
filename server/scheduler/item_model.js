var mongoose = require('mongoose');
var deepPopulate = require('mongoose-deep-populate')(mongoose);
const utils = require('./utils')


var SchedulerItemSchema = new mongoose.Schema({
	name: {
		type: String,
    },
    time_trigger:{
        type: Number
    },
    action: mongoose.Schema.Types.Mixed

});
// Get items between time range (timestamp format)
function getByTimeRange(begin, end) {
	return new Promise((resolve, reject) => {
		DB.SchedulerItem
			.where('time_trigger')
			.gt(begin)
			.lt(end)
			.exec((_items) => {
				resolve(_items);
			})
			.catch(log.onerr)
	})
}

// Get all the items of today
SchedulerItemSchema.static('getToday', function() {
	return new Promise((resolve,reject) => {

		const midnight_today = 		utils.todayAt(0,0,0)
		const midnight_tomorrow = utils.todayAt(23,59,59);

		getByTimeRange(midnight_today, midnight_tomorrow)
			.then((_items) => {
				resolve(_items)
			})
	})
})

// Get items in a specific time range (min-max)
SchedulerItemSchema.static('getByTime', function(begin, end) {
	return getBytTimeRange(begin,end)
})

SchedulerItemSchema.plugin(deepPopulate);
module.exports = { model: mongoose.model('SchedulerItem', SchedulerItemSchema), schema: SchedulerItemSchema };


