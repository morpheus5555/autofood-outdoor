

const ScheduleItem = function(name, action, time_trigger, time_estimated) {
    this.id = null
    this.name = name
    this.action = action
    this.time_trigger = time_trigger
    this.time_estimated = time_estimated

    this.checkTime();
}
ScheduleItem.prototype.trigger = function() {
    //if(!this.name)
    log.info(`${this.action.name} (${this.name}) has been triggered`)

    require('../actions/actionManager').incomingAction(this.action) // must be moved from here
}
ScheduleItem.prototype.hasActionObject = function() {
    return this.action_object !== undefined && this.action_object !== null
}
ScheduleItem.prototype.checkTime = function() {
    const relative_time_trigger =  this.time_trigger- Date.now()
    setTimeout(this.trigger.bind(this), relative_time_trigger)
}

module.exports = ScheduleItem