




const connectToMongo = () => mongoose.connect(`mongodb://localhost/bot`, {
	useMongoClient: true,
	promiseLibrary: Promise	
})

module.exports = () => new Promise((resolve,reject) => {
	log.info('Connection to MongoDB...')

	connectToMongo();

	mongoose.connection.on('open', () => {
		log.info("Connected to MongoDB")
		resolve();
	});

	mongoose.connection.on('error', err => {
		log.err(err);
		reject(err);
	});

	mongoose.connection.on('disconnected', connectToMongo);
})
