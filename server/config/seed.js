/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var Ground = require("../models/ground.js");


Ground.find({}).remove(function() {
    Ground.create({
        name: 'Seed User',
        email: 'seed@test.com',
        image: 'http://'
    });
});
