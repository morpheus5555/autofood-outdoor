var msg = {
  err: {
    user_not_logged_in: "You're not logged in",
    user_logged_in: "You're already logged in",

    user_login_email: "That user doesn't exist",
    user_login_password: "Wrong password",
    user_signup_email: "Invalid email",
    user_signup_email_taken: "That email is already taken",
    user_signup_password_length: "Password must be between 6 and 20 characters",

    user_empty_inputs: "Empty email or password",

    invalid_route: "Invalid route or undefined item"
  },
  win: {
    user_logged_in: "Logged in",
    user_signed_up: "Registered, you can now log in"
  }
}

module.exports = msg;
