/**
 * Express configuration
 */

'use strict';

var express = require('express');
var favicon = require('serve-favicon');
var compression = require('compression');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var path = require('path');
var config = require('./environment');
var fs = require('fs');
var session = require('express-session');
var passport = require('passport');
var MongoStore = require('connect-mongo/es5')(session);
var flash = require('express-flash');

module.exports = function (app) {
    var env = app.get('env');

    // app.use(session({
    //   store: new MongoStore({
    //     mongooseConnection: mongooseconn
    //   }),
    //   secret: 'AuToFoOd 123',
    //   name: 'seed.sid',
    //   cookie: {maxAge: 3600000, secure: false},
    //   resave: false,
    //   saveUninitialized: true
    // }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(flash());
    app.use(compression());
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(bodyParser.json());
    app.use(methodOverride());
  //  app.use(favicon(path.join(config.root, config.appPath, 'favicon.ico')));
    app.use(cookieParser());
    app.use(express.static(path.join(config.root, config.appPath)));
};
