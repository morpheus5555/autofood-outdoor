'use strict';

// Development specific configuration
// ==================================
module.exports = {
  db: {
    uri:'mongodb://localhost/bot'
  },
  appPath:"server",
  seedDB: false
};
