const BotModel = require('mongoose').model('Bot');
;

function botExist() {
	return new Promise((resolve, reject) => {
		BotModel.find({}, (err, b) => {
			resolve({exist: b.length >= 1, bot: b[0]});
		});
	});
}


function create() {
	return new Promise((resolve, reject) => {
		botExist().then(r => {

			if(r.exist) {
				return r.bot.resetHexPosition(() => {
					resolve();
				});
			}

			log.info('No bot found in the database, Creating one...');

			const bot = new BotModel();

			bot.addFoodTypes(bot)
				.then(bot.addPumps)
				.then(bot.addAxis)
				.then(bot.addMotors)
				.then(bot.addGround)
				.then(bot.addHead)
				.then(bot.addToolBase)
				.then(bot.addScheduler)
				.then(bot.saveAndCreate)
				.then(() => {
					resolve();
				})
				.catch(err => {
					log.err(err);
					reject(err);
				});
		});
	});
}

module.exports = {
	botExist,
	create
}
