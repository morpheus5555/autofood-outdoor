'use strict';

var express = require('express');
var mongoose = require('mongoose');

module.exports = function(controller) {
  var router = express.Router({mergeParams: true});

  router.get('/', controller.index);
  router.post('/', controller.post);
  router.put('/:id', controller.put);
 // router.get('/:id', controller.getById);
  router.delete('/:id', controller.rmById);
  router.post('/rm', controller.rmMultiple);

 
  return router;
}
