'use strict';

var mongoose = require('mongoose');
var baseRoute = require('../baseRoute');
let Controller = require('./../baseController')

let controller = new Controller('Bot', null, [], [
    'scheduler',
    'scheduler.items',
    'scheduler.history',
    'foodTypes',
    'foods.type',
    'axis.x',
    'axis.x.groups',
    'axis.y',
     'axis.y.groups',
    'axis.z',
    'axis.z.groups',
    'axis.a',
    'axis.a.groups',
    'head',
    'head.tool',
    'toolBase',
    'motors',
    'pumps'
  ]);
 
 controller.getById = function(req, res){
    process.nextTick(function () {
      if(parents.length > 0) {
        parentsExist(req, mdl_array_name, parents).then(function(ids) {
          if(ids) {
            var contains = containsId(ids, req.params.id);
            if(contains) {
              Model.findById(req.params.id, function(err, mdl){
                if (err) return res.status(500).json({ success: false, msg: err });

                deepPopulate(Model, mdl, children, res);
              });
            } else {
              res.status(500).json({ success: false, msg: msg.err.invalid_route });
            }
          }
        }).catch(function(err){
          res.status(500).json({ success: false, msg: err });
        });
      } else {
        Model.findById(req.params.id, function(err, mdl) {
          if (err) return res.status(500).json({ success: false, msg: err });

          deepPopulate(Model, mdl, children, res);
        });
      }
    });
  }


const router = baseRoute(controller);

 router.post('/reset', function(req,res) {

 })

module.exports = router;
