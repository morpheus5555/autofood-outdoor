'use strict';

var mongoose = require('mongoose');
var baseRoute = require('../baseRoute');
let Controller = require('./../baseController')

let controller = new Controller('Shelf', 'shelves', [{
	'model': 'Bot',
	'id': 'bot_id',
	'arr': 'bots'
}], []);

module.exports = baseRoute(controller);


