'use strict';

var mongoose = require('mongoose');
var baseRoute = require('../baseRoute');
let Controller = require('./../baseController')

let controller = new Controller('Axis', 'axis', [{
	'model': 'Bot',
	'id': 'bot_id',
	'arr': 'bots'
}], [
'groups'
]);

module.exports = baseRoute(controller);


