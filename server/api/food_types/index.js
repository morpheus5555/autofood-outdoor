'use strict';

var _ = require('underscore');
var mongoose = require('mongoose');

var FoodType = require('../../models/foodtype').model;

let Controller = require('./../baseController')
let controller = new Controller('FoodType', 'foodTypes', [{
  'model': 'Bot',
  'id': 'bot_id',
  'arr': 'bots'
}], []);

var baseRoute = require('../baseRoute')(controller);

var Promise = require('bluebird');



// remove foodtypes doublons (of bot foods.foodtypes array by example)
var filterFoodTypes = Promise.promisify(function (foodtypes, callback) {
  callback(0, foodtypes.reduce((processed, foodtype) => { // returning directly the Promise success callback (certainly not the best)
    if (!_.contains(processed, foodtype)) {
      processed.push(foodtype);
      return processed;
    }
  }, []));
});

function addIfUniq(foodtypes) {
  foodtypes.map(function (f, i, arr) {
    controller.getModel().findOne({ "name": f.name }, function (err, ft) {
      if (ft === null) {
        var new_foodtype = new FoodType(f);
        new_foodtype.save(function (err) {
          //          if (err) return handleError(err);
          log.info('foodtype', f, 'has been added.');
        })
      }
    });
  });
}

baseRoute.post('/adduniq', function (res, req) {
  var _foodtypes = req.req.body;
  filterFoodTypes(_foodtypes)
  .then(addIfUniq);
  //  .catch(e => {
  //   log.info(e);
  //});
});

//controller.index = function(req,res) {
//}

module.exports = baseRoute;
