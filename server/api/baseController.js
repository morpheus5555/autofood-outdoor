'use strict';

var _ = require('underscore');
var mongoose = require('mongoose');
var async = require('async');

var msg = require('./../config/msg');
var createBot = require('./../createBot').create;

;

// change (recursively) id to their object
function deepPopulate(Model, mdl, children, res) {
  Model.deepPopulate(mdl, children, function(err, _mdl) {
    if (err) return res.status(500).json({ success: false, msg: err });

    res.json({ success: true, msg: mdl });
  });
}

function containsId(arr, id) {
  for(var i = 0; i < arr.length; ++i) {
    if(arr[i].equals(id))
    return true;
  }

  return false;
}

// Check if parents id are set in the uri
function parentsIdAreSet(parents, req) {
  for(var i = 0; i < parents.length; ++i) {
    if(req.params[parents[i].id] === undefined || req.params[parents[i].id] === undefined) {
      return false;
    }
  }

  return true;
}

// Return the ids of Model available relatively to its parent set in the uri
function parentsExist(req, mdl_array_name, parents) {
  return new Promise(function(resolve, reject) {
    var last_parent;

    // For each parent
    async.eachOfSeries(parents, function(parent, i, callback) {
      // If the id isn't set in uri, reject
      if(req.params[parent.id] === undefined || req.params[parent.id] === null)
        return reject(msg.err.invalid_route);

      // If this parent's id wasn't inside the last, reject
      if(i > 0 && !containsId(last_parent[parent.arr], req.params[parent.id]))
        return reject(err);

        

      // Find the parent
      mongoose.model(parent.model).findById(req.params[parent.id], function(err, p_mdl) {
        if (err) return reject(err);

        

        // Set the parent to last_parent
        last_parent = p_mdl;

        // Go to next iteration
        callback();
      });
    }, function(err) { // When it's all over or an error occured
        if (err) return reject(err);

        // Send the ids
        resolve(last_parent[mdl_array_name]);
    });
  });
}

class baseController {
  constructor(model, mdl_array_name, parents, children, functions, callbacks) {
    this.Model = mongoose.model(model);
    this.model_name = model;
    this.mdl_array_name = mdl_array_name 
    this.parents = parents;
    this.children = children;
    this.functions = functions;
    this.callbacks = callbacks;

    _.bindAll(this, 'index', 'postParents', 'post', 'put', 'getById', 'rmById', 'rmMultiple');
  }
  setCallbacks(_callbacks) {
    this.callbacks = _callbacks
    log.info(this.callbacks);
  }
  // Get all Models
  index(req, res){
    log.info('Get all ' + this.model_name);
    process.nextTick(() => {
      // If got parents and their ids are set in the uri (e.g. /grounds/123/bots)
      if(this.parents.length > 0 && parentsIdAreSet(this.parents, req)) {
        parentsExist(req, this.mdl_array_name, this.parents).then(function(ids) {
          if(ids) {
            this.Model.find({"_id": { "$in": ids }}, function(err, mdls) {
              if (err) return res.status(500).json({ success: false, msg: err });

              if(this.callbacks.onfinded !== undefined) this.callbacks.onfinded(mdls);

              deepPopulate(Model, mdls, this.children, res);
            });
          } else {
            res.status(500).json({ success: false, msg: msg.err.invalid_route });
          }
        }).catch(function(err) {
          res.status(500).json({ success: false, msg: err });
        });
      }
      //
      // If is the root (doesn't got parents), or want to get all collection (e.g. /grounds/bots)
      else {
        this.Model.find({}, (err, mdl) => {
          if (err) return res.status(500).json({ success: false, msg: err });

          if(mdl === undefined || mdl === null || mdl.length === 0) {
          	createBot().then(r => {
							if(r) res.json(r);
						});
          }else{
            deepPopulate(this.Model, mdl, this.children, res);
          }
        });
      }
    });
  }

  postParents(req,res,data) {
    parentsExist(req, this.mdl_array_name, this.parents).then((ids) => {
      this.Model.create(data, (err, mdls) => {
        if (err) return res.status(500).json({ success: false, msg: err });
		
		var mdls_id = [];
		if(mdls.constructor.name === "Array") {
			for(var i = 0; i < mdls.length; ++i) {
				mdls_id.push(mdls[i]._id);
			}
		} else {
			mdls_id.push(mdls);
		}
		var id = req.params[this.parents[this.parents.length - 1].id];
		var update = { "$push": { [this.mdl_array_name]: { "$each": mdls_id }}};

		// Update the parent (push newModel's id to mdl_array_name)
		mongoose.model(this.parents[this.parents.length - 1].model).findByIdAndUpdate(id, update, function(err, p_mdl) {
			if (err) return res.status(500).json({ success: false, msg: err });

			res.json({ success: true, msg: mdls });
		});
      });
    }).catch(function(err) {
      res.status(500).json({ success: false, msg: err });
    });
  }

  // Create new Model
  post(req, res){
    
    process.nextTick(() => {
      var data = _.extend(req.body);
      var newModel = new this.Model(data);

      if(this.functions !== undefined && this.functions.postModel !== undefined) {
        this.functions.postModel(newModel);
      }

      if(this.parents.length > 0) {
        this.postParents(req,res,data);
      } else {
        newModel.save((err, mdl) => {
          if (err) return res.status(500).json({ success: false, msg: err });

          res.json({ success: true, msg: mdl });
        });
      }
    });
  }

  // Update Model
  put(req, res){
    process.nextTick(() => {
      if(this.parents.length > 0) {
        parentsExist(req, this.mdl_array_name, this.parents).then((ids) => {
          this.Model.findByIdAndUpdate(req.params.id, req.body, (err, mdl) => {
            if (err) return res.status(500).json({ success: false, msg: err });

            res.json({ success: true });
          });
        }).catch(function(err){
          res.status(500).json({ success: false, msg: err });
        });
      } else {
        this.Model.findByIdAndUpdate(req.params.id, req.body, (err, mdl) => {
          if (err) return res.status(500).json({ success: false, msg: err });

          res.json({ success: true });
        });
      }
    });
  }

  // Get by id
  getById(req, res){
    process.nextTick(() => {
      if(this.parents.length > 0) {
        parentsExist(req, this.mdl_array_name, this.parents).then((ids) => {
          if(ids) {
            var contains = containsId(ids, req.params.id);
            if(contains) {
              this.Model.findById(req.params.id, (err, mdl) => {
                if (err) return res.status(500).json({ success: false, msg: err });

                deepPopulate(this.Model, mdl, this.children, res);
              });
            } else {
              res.status(500).json({ success: false, msg: msg.err.invalid_route });
            }
          }
        }).catch(function(err){
          res.status(500).json({ success: false, msg: err });
        });
      } else {
        this.Model.findById(req.params.id, function(err, mdl) {
          if (err) return res.status(500).json({ success: false, msg: err });

          deepPopulate(this.Model, mdl, this.children, res);
        });
      }
    });
  }

  // Remove by id
  rmById(req, res){
    process.nextTick(() => {
      if(this.parents.length > 0) {
        parentsExist(req, this.mdl_array_name, this.parents).then((ids) => {
          this.Model.findByIdAndRemove(req.params.id, (err, mdl) => {
            if (err) return res.status(500).json({ success: false, msg: err });

            res.json({ success: true });
          });
        }).catch((err) => {
          res.status(500).json({ success: false, msg: err });
        });
      } else {
        this.Model.findByIdAndRemove(req.params.id, (err, mdl) => {
          if (err) return res.status(500).json({ success: false, msg: err });

          res.json({ success: true });
        });
      }
    });
  }

  // Remove multiple
  rmMultiple(req, res) {
  	process.nextTick(() => {
		if(this.parents.length > 0) {
			parentsExist(req, this.mdl_array_name, this.parents).then((ids) => {
				this.Model.find({ "_id": { "$in": req.body}}).remove((err, mdls) => {
					if (err) return res.status(500).json({ success: false, msg: err });

					var id = req.params[this.parents[this.parents.length - 1].id];
					var update = { "$pullAll": { [this.mdl_array_name]: req.body }};
					mongoose.model(this.parents[this.parents.length - 1].model).findByIdAndUpdate(id, update, (err, p_mdl) => {
						if (err) return res.status(500).json({ success: false, msg: err });

						res.json({ success: true });
					});

				});
			}).catch((err) => {
				res.status(500).json({ success: false, msg: err });
			});
		} else {
			this.Model.remove(req.body, (err, mdls) => {
				if (err) return res.status(500).json({ success: false, msg: err });

				res.json({ success: true });
			});
		}

	});
  }
  getModel() {
    return this.Model;
  }
}

module.exports = baseController;
