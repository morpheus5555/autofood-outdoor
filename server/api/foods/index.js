'use strict';

var _ = require('underscore');
var async = require('async');

var mongoose = require('mongoose');

let food_types = require('../../models/foodtype');


let Controller =  new require('./../baseController');

let model_name = 'Food';
let model_name_array = 'foods';
let parents = [{
  'model': 'Bot',
  'id': 'bot_id',
  'arr': 'bots'
}];
let children = ['type'];

let functions = {
  postModel: function (model) {

  },
}

var controller = new Controller(model_name,
  model_name_array,
  parents,
  children,
  functions,
  {
    onfinded: (foods) => {
      log.info('ONFINDED');
      return controller
        .getModel()
        .update_percentage(foods)
}
});

var baseRoute = require('../baseRoute')(controller);

baseRoute.get('/searchBy', function (req, res) {
  log.info(req.query);

  var type = req.query.type;
  var name = req.query.name;
  var order = Number(req.query.order) || 1;



  var user_input = new RegExp(name, 'i');
  var queryArray = [];
  for (var property in controller.getModel().schema.paths) {
    if (controller.getModel().schema.paths.hasOwnProperty(property)  && controller.getModel().schema.paths[property]["instance"] === "String") {
      queryArray.push(JSON.parse('{\"' + property + '\": \"' + user_input + "\"}"))
    }
  }

  var query = {
    $or: queryArray
  };


  var type_sort = {}
  if(type === "name") // todo: improve this when sc?
    type_sort = { "type.name": order }
  else if(type === "time")
    type_sort = { /*"type.name": order,*/ "type.time.percentage": order }
  else if(type === "price")
    type_sort = { "type.name": order, "type.price.eur": order }
  else
    log.err('No type sort provided by the search request');

  var r = {};
  if (name !== null && name !== '') {
    r = { 'name': new RegExp(name, "i") }
  }

  food_types.model.find(r, function (err, types) {
    var foods = []
    let types_id = _.pluck(types, '_id')

    controller.getModel()
      .aggregate([
        //{
        //  $match: {
        //    "type": {
        //      $in: types_id
        //    }
        //  },
          
        //},
        //query,
        {
          $lookup:
          {
            localField: "type",
            from: 'foodtypes',
            foreignField: "_id",
            as: "type"
          },
        },
        { $unwind: "$type" },
        {
          $sort: type_sort

        },
     //   { $limit: 100 }
      ])
      .exec(function (err, _foods) {
        if(err) log.info(err);

        if(controller.callbacks.onfinded) {
          controller.callbacks.onfinded(_foods).then(function(updated_foods) {
            log.info(updated_foods);
            res.status(200).json({ success: true, foods: updated_foods });
          }).catch((err) => {
            log.info(err);
          });
        } else {
          res.status(200).json({ success: true, foods: _foods });
        }
        
      })
  });
});

module.exports = baseRoute;