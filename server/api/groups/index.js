'use strict';

var mongoose = require('mongoose');
var baseRoute = require('../baseRoute');
let Controller = require('./../baseController')

let controller = new Controller('Group', 'groups', [{
	'model': 'Axis',
	'id': 'axis_id',
	'arr': 'axis'
},
{
	'model': 'Bot',
	'id': 'bot_id',
	'arr': 'bots'
}], []);

module.exports = baseRoute(controller);