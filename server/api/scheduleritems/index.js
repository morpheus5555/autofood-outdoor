'use strict';

var mongoose = require('mongoose');
var baseRoute = require('../baseRoute');
let Controller = require('./../baseController')

let controller = new Controller('SchedulerItem', 'scheduleritem', [
{
	'model': 'Scheduler',
	'id': 'scheduler_id',
	'arr': 'schedulers'
}], []);

module.exports = baseRoute(controller);