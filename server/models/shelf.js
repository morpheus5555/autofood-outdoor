'use strict';

var mongoose = require('mongoose');
var deepPopulate = require('mongoose-deep-populate')(mongoose);

var ShelfSchema = new mongoose.Schema({
	height: {
		type: Number,
		default: 120
	},
	category: {
		type: String,
		default: 'grow'
	},
	offset: {
		type: Number,
		default: 80
	},
	columns: [{
		direction: {
			type: String,
			default: 'back'
		},
		rows: [{
			width: {
				type: Number,
				default: 40
			},
			offset: {
				type: Number,
				default: 20
			}
		}]
	}]
});

ShelfSchema.plugin(deepPopulate);

module.exports = {
	model: mongoose.model('Shelf', ShelfSchema),
	schema: ShelfSchema
};

