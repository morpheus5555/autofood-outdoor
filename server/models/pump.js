
const deepPopulate = require('mongoose-deep-populate')(mongoose);
const initFile = __dirname + '/init/pump.json';
;
const fs = require('fs')

const PumpSchema = new mongoose.Schema({
	name: String,
	volPerSec: Number // milliliters/sec
});

PumpSchema.static('createDefaults', () => new Promise((resolve,reject) => {
	log.info('Creating Pumps...');

	fs.readFile(initFile, 'utf8', (err, data) => {
		if(err) return reject(err);

		DB.Pump
			.create(JSON.parse(data))
			.then((pumps) => {
				log.info('Pumps created!');
				resolve(pumps);
			})
	})
}));

PumpSchema.static('volumeToSec', function(props) {
	return DB.Pump.findOne({ name: props.name })
		.then(pump => {
			if(!pump) return null;

			return props.volume / pump.volPerSec;
		})
})

PumpSchema.plugin(deepPopulate);
module.exports = { model: mongoose.model('Pump', PumpSchema), schema: PumpSchema };

