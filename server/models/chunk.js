
const deepPopulate = require('mongoose-deep-populate')(mongoose);


const chunkSize = 2; // in row of slot

const ChunkSchema = new mongoose.Schema({
	index: Number,
	slots: [{
		type: mongoose.Schema.Types.ObjectId, ref: 'Slot'
	}]
});

ChunkSchema.static('seed', (foodProps) => new Promise((resolve, reject) => {
	const slotPos = foodProps.position;
	const chunkIndex = Math.floor(slotPos.y / chunkSize)

	// TODO
	// return mongoose.model('Chunk')
	// 	.findOne({ index: chunkIndex })
	// 	.populate({
	// 		path: 'slots',
	// 		match: { 'position.x': slotPos.x }
	// 	})
	// 	.then((chunk, err) => {
	// 		chunk.slots.filter(function(slot){
	// 			return slot.length;
	// 		  })
	// 	})
}));

ChunkSchema.static('createAndInsertSlots', (groundSize) => new Promise((resolve, reject) => {
	return DB.Slot
		.find({})
		.then(slots => {
			const size = Math.ceil(groundSize.y / chunkSize)
			let chunks = Array(size)
				.fill()
				.map((el, index) => ({
					index,
					slots: []
				}))

			slots.forEach(slot => {
				const index = Math.floor(slot.position.y / chunkSize);
				chunks[index].slots.push(slot._id);
			})

			return chunks;
		})
		.then(chunks => {
			return DB.Chunk
				.insertMany(chunks)
				.then(() => {
					log.info(`${chunks.length} Chunks created!`)
					resolve()
				})
		})
}));

ChunkSchema.plugin(deepPopulate);
module.exports = { model: mongoose.model('Chunk', ChunkSchema), schema: ChunkSchema };