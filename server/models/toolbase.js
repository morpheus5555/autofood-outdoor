
const deepPopulate = require('mongoose-deep-populate')(mongoose);

const initFile = __dirname + '/init/toolbase.json';


const ToolBaseSchema = new mongoose.Schema({
	toolSpacing: { // cm
		type: Number, 
		default: 20
	},
	position: { // step
		x: {
		  type: Number,
		  default: 3000,
		},
		z: {
		  type: Number,
		  default: 2000
		}
	},
	 // The Size of the tool plank (In CM)
	size: {
		x: {
			type: Number,
			default:50.0,
		},
		y: {
			type: Number,
			default: 25.0,
		},
	}
});

ToolBaseSchema.static('getPosition', function() {
	return DB.ToolBase
		.findOne().lean()
		.then(toolbase => toolbase.position)
})

// return position in step on the x axis of tool `toolName` on the toolBase
ToolBaseSchema.static('getPositionOfTool', function(toolName) {
	return Promise.all([
		DB.ToolBase.findOne().lean(),
		DB.Tool.find({}).lean(),
		DB.Axis.findOne({ name: 'x' }),
	]).then(([toolBase, tools, axis]) => {
		const index = tools.findIndex(tool => tool.name === toolName);

		if(index === -1) {
			log.err(`Tool ${toolName} doesn't exist`)
			return null;
		}

		return toolBase.position.x + (toolBase.toolSpacing * axis.stepPerCM) * index;
	})
})

ToolBaseSchema.plugin(deepPopulate);

module.exports = { model: mongoose.model('ToolBase', ToolBaseSchema), schema: ToolBaseSchema };

