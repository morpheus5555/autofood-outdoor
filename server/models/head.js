var mongoose = require('mongoose');
var deepPopulate = require('mongoose-deep-populate')(mongoose);
const Seeder = require('./seeder')

const HeadSchema = new mongoose.Schema({
    // The A Axis (Head Roation)
    axis: { type: mongoose.Schema.Types.ObjectId, ref: 'Axis' },

    // The Current Tool
    tool: { type: mongoose.Schema.Types.ObjectId, ref: 'Tool' }
});

/**
 * Check if current tool name on head is `tool_name`
 * @param {String} tool_name 
 */
HeadSchema.statics.hasTool = (tool_name) => 
    Promise.all([
        DB.Head.findOne(),
        DB.Tool.findOne({ name: tool_name })
    ]).then(([head, tool]) => {
        log.e(is.undefined(tool), `Tool '${tool_name}' not found`)
        if(!head.tool) return false
        return head.tool.toString() === tool._id.toString()
    })


HeadSchema.statics.getCurrentTool = () => 
    DB.Head
    .findOne()
    .populate('tool')
    .lean()
    .then(head =>  head.tool)

/**
 * Change the  {@link Head#tool} that correspond to the id
 * of actual equiped tool 
 * @param {String} tool_name 
 */
HeadSchema.statics.selectTool = (tool_name) => {
    if(!tool_name || tool_name === "Nothing")
        DB.Head.findOne().then(head => {
            head.tool = undefined;
            head.save()
        })
    else
        DB.Tool.findOne({ name: tool_name })
        .then((tool) => {
            if(!tool) log.err("Tool not found !")
            else DB.Head.findOne().then(head => { 
                head.tool = tool._id;
                head.save()
            }) 
        })
}
// Create Tools
HeadSchema.methods.addTools = (head) => 
    Seeder.model
    ._create()
    .then(() => head)

HeadSchema.statics._create = () => 
    DB.Axis
    .findOne({ name: 'a' })
    .then(axis => DB.Head.create({ axis: axis._id }))
    .then(head => head.addTools(head))

    // (Temporary)
    .then(head => { DB.Head.selectTool("Seeder"); return head })


HeadSchema.plugin(deepPopulate);

module.exports = { model: mongoose.model('Head', HeadSchema), schema: HeadSchema };
