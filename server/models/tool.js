var mongoose = require('mongoose');
var deepPopulate = require('mongoose-deep-populate')(mongoose);


var ToolSchema = new mongoose.Schema({
    name: { type: String }
});

ToolSchema.plugin(deepPopulate);

module.exports = { model: mongoose.model('Tool', ToolSchema), schema: ToolSchema };
