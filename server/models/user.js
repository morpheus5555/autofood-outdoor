'use strict';

var mongoose = require('mongoose');
var deepPopulate = require('mongoose-deep-populate')(mongoose);
var bcrypt   = require('bcrypt-nodejs');

var UserSchema = new mongoose.Schema({
  email: String,
  password: String
});

UserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

UserSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

UserSchema.plugin(deepPopulate);

module.exports = mongoose.model('User', UserSchema);
