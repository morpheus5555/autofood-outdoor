'use strict';

var _ = require('underscore');
var mongoose = require('mongoose')
var deepPopulate = require('mongoose-deep-populate')(mongoose)
var Promise = require('bluebird');

var FoodSchema = new mongoose.Schema({
  bot_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Bot' },
  name: String,
  type: { type: mongoose.Schema.Types.ObjectId, ref: 'FoodType' },
  position: {
    x: Number,
    y: Number
  },
  time: {
    planted: { type: Date, default: Date.now() },
    recolted: { type: Number, default: 0 },
    percentage: { type: Number, default: 0 },
    last_updated: { type: Date, default: Date.now() }
  },

  // snapshots: [{
  //   type: mongoose.Schema.Types.ObjectId, ref: 'FoodSnapshot'
  // }]
})

//FoodSchema.virtual('time.percentage').get(function () {
//  var target = Date.now() + this.type.time * 86400000
//  return (((Date.now() - new Date(this.time.planted).getTime()) / (target - new Date(this.time.planted).getTime())) * 100).toFixed(2);
//})

FoodSchema.static('createNew', (foodProps) => new Promise((resolve,reject) => {
	DB.FoodType
		.findOne({ name: foodProps.type })
		.exec((err, foodType) => {
			if(err) return reject(err);
			if(!foodType) return reject(`FoodType \'${foodProps.type}\' doesn\'t exist`);

			DB.Food
				.create({
					'name': foodProps.name,
					'position.x': foodProps.position.x,
					'position.y': foodProps.position.y,
					'type': foodType._id
				})
				.then((food) => {
					resolve(food);
				})
		})
}));

FoodSchema.static('createMany', (foodsProps) => new Promise((resolve,reject) => {
	return DB.Food
		.insertMany(foodsProps, (err, foods) => {
			log.info(err)
			resolve(foods)
		})
}));

function calculatePercentage(planted_on, max_time) {
  var target = Date.now() + max_time * 86400000;
  planted_on = new Date(planted_on).getTime();
  let time_elapsed = Date.now() - planted_on;
  let time_left = target - planted_on;
  return (time_elapsed / time_left * 100).toFixed(2);
}

// Update the time.percentage value of a food when a user request it,
// Only if it last update Xsec/min ago
FoodSchema.statics.update_percentage = Promise.promisify(function (foods, callback) {
  foods.forEach((food, index) => {
      // Little optimization here.
      // We update only the food.time last_updated 60sec ago 
      if(Date.now() - new Date(food.time.last_updated).getTime() > 0) {

        this.findOneAndUpdate({ _id: food._id}, 
                    { "$set": {"time.percentage": calculatePercentage(food.time.planted, food.type.time),
                      "time.last_updated": Date.now()}}, // Reset the last updated value
          function (err, _food) {
            if (err) return handleError(err);

            // manually set the variable just for this response to the client
            // otherwise it will reload a second time for seeing the document modifications.
            //
            foods[index].time.percentage = _food._doc.time.percentage; 
        }
      )
    }
    if(index >= foods.length - 1) // success callback promise when the last food has been updated
        callback(0, foods)
  });
});


FoodSchema.static('isTypeUsed', (type_id) => {
  return DB.Food.findOne({
    type: type_id
  }).then(food => {
    return food ? true : false;
  })
})


FoodSchema.options.toJSON = { virtuals: true }

FoodSchema.plugin(deepPopulate)

FoodSchema.index({ '$**': 'text' })

let FoodModel = mongoose.model('Food', FoodSchema)

module.exports = { model: FoodModel, schema: FoodSchema }
