'use strict';

var _ = require('underscore');
var mongoose = require('mongoose')
var deepPopulate = require('mongoose-deep-populate')(mongoose)
var Promise = require('bluebird');
var async = require('async');

var SlotSchema = new mongoose.Schema({
	food: { type: mongoose.Schema.Types.ObjectId, ref: 'Food' },
	state: {
		name: {
			type: String,
			default: 'empty',
			index: true
		},
		type: {
			type: String,
			default: 'none'
		},
	},
	position: {
		x: { type: Number, index: true },
		y: { type: Number, index: true }
	},
})


SlotSchema.static('getEmpties', () => {
    return DB.Slot
			.find({ 'state.name': 'empty' })
			.lean();
});

SlotSchema.static('seed', (foodProps) => new Promise((resolve,reject) => {
	DB.Food
		.createNew(foodProps)
		.then((food) => {
			DB.Slot
				.updateOne({
					'position.x': food.position.x,
					'position.y': food.position.y
				}, {
					'food': food._id,
					'state.name': 'busy'
				},
				(err,_food) => {
					resolve(food)
				})
		})
}));

SlotSchema.static('seedMany', (foodsProps) => new Promise((resolve,reject) => {
	DB.Food
		.createMany(foodsProps)
		.then((foods) => {
			return async.map(foods, (food, cb) => {
				DB.Slot
					.updateOne({
						'position.x': food.position.x,
						'position.y': food.position.y
					}, {
						'food': food._id,
						'state.name': 'busy'
					}, cb);
			}, (err,f) => {
				resolve(f)
			});
		})
}));

SlotSchema.static('getPos', (slots) => new Promise((resolve,reject) => {
    let s = slots;
    return async.map(slots, (item) => {
        return item.position;
    }, (err, results) => resolve(results));
})
); 
/*SlotSchema.methods.get =  () => new Promise((resolve,reject) => {
    return this.find({ 'state.name': "empty" });
});
*/

SlotSchema.statics.hasFood = (position) => DB.Slot.findOne({ position }).then(slot => slot.state.name === 'busy') 

let SlotModel = mongoose.model('Slot', SlotSchema)

module.exports = { model: SlotModel, schema: SlotSchema }
