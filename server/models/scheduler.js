var mongoose = require('mongoose');
var deepPopulate = require('mongoose-deep-populate')(mongoose);

var SchedulerSchema = new mongoose.Schema({
    
    // Current items not already executed
    items:   [{ type: mongoose.Schema.Types.ObjectId, ref: 'SchedulerItem' }],
    
    // Simply the history of all the ancient (executed) items
   // useful for later analysis
   history: [{ type: mongoose.Schema.Types.ObjectId, ref: 'SchedulerItem' }]
   
}, {
    usePushEach: true
});

// Check if at least one Scheduler exist
SchedulerSchema.static('get', function() {
    return new Promise((resolve,reject) => {
        DB.Scheduler
            .find({})
            .then((schedulers) => {
                if(!schedulers || schedulers === null || schedulers.length === 0) {
                    resolve(null)
                    log.err("There is no existing scheduler in database")
                } else
                    resolve(schedulers[0])
            })
    })
})

// Check if 'items' _id array is not empty
SchedulerSchema.methods.isEmpty = function() {
	return this.items === undefined || this.items === null || this.items.length === 0      
}

// Add item _ID in the items array
SchedulerSchema.static('addItem', function(_item) {
    return new Promise((resolve,reject) => {
        DB.Scheduler.get()
        .then((scheduler) => {
            scheduler.items.push(_item.id)
            scheduler.save()
                .then((item) => {
                    resolve(item)
                })
            })
            .catch(log.onerr)
    })
})

// Add multiple items _id
SchedulerSchema.static('addItems', function(_items) {
    return new Promise((resolve,reject) => {
        
    })
})



SchedulerSchema.plugin(deepPopulate);
module.exports = { model: mongoose.model('Scheduler', SchedulerSchema), schema: SchedulerSchema };

