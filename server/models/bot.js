'use strict';

var mongoose = require('mongoose');
var deepPopulate = require('mongoose-deep-populate')(mongoose);
var Promise = require('bluebird');
const utils = require('../actions/utils');
const async = require('async');
const fs = require('fs');

const axisInitFile = __dirname + '/init/axis.json';
const motorsInitFile = __dirname + '/init/motors.json';
mongoose.Promise = Promise;

Promise.promisifyAll(fs);

var BotSchema = new mongoose.Schema({
  name: String,
  serial: [],
  scheduler: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Scheduler'
  },
  hexPosition: {
    x: {
      type: Number,
      default: null
    },
    y: {
      type: Number,
      default: null
    }
  },
  motors: [{
    type:  mongoose.Schema.Types.ObjectId,
    ref: 'Motor'
  }],
  axis: {
    x: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Axis'
    },
    y: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Axis'
    },
    z: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Axis'
    },
    a: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Axis'
    },
  },
  head: {
    type: mongoose.SchemaTypes.ObjectId,
    ref: 'Head'
  },
  toolBase: {
    type: mongoose.SchemaTypes.ObjectId,
    ref: 'ToolBase'
  },
  pumps: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Pump'
  }],
  ground: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Ground'
  },
  foods: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Food'
  }],
  foodTypes: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'FoodType'
  }]
}, {
  usePushEach: true
});

BotSchema.pre('save', (next) => {
  if (!this.axis) {
    //log.info(this);
    //	this.axis.push({ name: "x" });
  }
  next();
});


BotSchema.methods.getNextFood = function (dir, cb) {
  if (!this.hexPosition ||
    this.hexPosition.x === null ||
    this.hexPosition.y === null) {
    return cb('no_pos');
  }

  const even = (y) => y % 2 === 0;

  const foodsOnSameY = (y) => (
    even(y) ? {
      'position.x': {
        $gt: this.hexPosition.x
      }
    } : {
      'position.x': {
        $lt: this.hexPosition.x
      }
    })

  // Get nearest Y which got food 
  const getNearestY = (_cb) => this.model('Food')
    .find({
      $or: [{
          'position.y': {
            $gt: this.hexPosition.y
          }
        },
        {
          $and: [{
              'position.y': this.hexPosition.y
            },
            foodsOnSameY(this.hexPosition.y)
          ]
        }
      ]
    })
    .sort({
      'position.y': 1
    })
    .limit(1)
    .exec((err, foods) => {
      if (foods.length <= 0) return cb();

      _cb(foods[0].position.y);
    });

  // Get nearest food using nearest Y 
  const getNearestFood = (nearestY) => this.model('Food')
    .find({
      $and: [{
          'position.y': nearestY
        },
        (nearestY === this.hexPosition.y ? foodsOnSameY(nearestY) : {})
      ]
    })
    .sort({
      'position.x': even(nearestY) ? 1 : -1
    })
    .limit(1)
    .exec((err, foods) => {
      cb(foods[0]);
    });


  return getNearestY(getNearestFood);
}

BotSchema.methods.setHexPosition = function (pos, cb) {
  this.hexPosition.x = pos.x;
  this.hexPosition.y = pos.y;

  this.save(function (err, bot) {
    cb(bot);
  });
}
BotSchema.methods.resetHexPosition = function (cb) {
  this.setHexPosition({
    x: null,
    y: null
  }, cb);
}

BotSchema.methods.deleteFoodType = function(id) {
  return DB.FoodType.findOne({ _id: id })
  .then(foodType => foodType.remove()) // used to trigger remove hook
  .then(() => {
    this.foodTypes.splice(this.foodTypes.indexOf(id), 1);
    return this.save();
  })
}

/* 
  ON CREATION FUNCTIONS
*/

BotSchema.methods.addFoodTypes = function (bot) {
  return DB.FoodType
    .createDefaults()
    .then(foodtypes => {
      bot.foodTypes = foodtypes;
      return bot;
    })
}
BotSchema.methods.addFoodType = function (foodTypeProps) {
  return DB.FoodType
    .create(foodTypeProps)
    .then(foodType => {
      this.foodTypes.push(foodType._id)
      return this.save().then(() => {
        return foodType;
      });
    })
}

BotSchema.methods.addPumps = function(bot) {
	return DB.Pump
		.createDefaults()
		.then(pumps => {
			bot.pumps = pumps;
			return bot;
		})
}

// Automatically add axis on bot creation
BotSchema.methods.addAxis = (bot) => {
  const getAxisProps = () => fs.readFileAsync(axisInitFile, 'utf8')
    .then((data) => {
      return JSON.parse(data);
    });

  const createAxes = (axes_props) => {
    const addGroups = (axes) => Promise.all(axes.map((axis, i) => {
      return axis.createGroups(axes_props[i]);
    }))

    return DB.Axis
      .insertMany(axes_props)
      .then(addGroups)
      .then((axes) => {
        axes.forEach(axis => {
          bot.axis[axis.name] = axis._id;
        })

        return bot;
      });
  }

  return getAxisProps().then(createAxes)

}

BotSchema.methods.addMotors = bot => {
  const getMotorsProps = () => 
    fs.readFileAsync(motorsInitFile, 'utf8')
      .then((data) => {
        return JSON.parse(data);
      });

  const createMotors = (props) => {
    return DB.Motor
      .insertMany(props)
      .then(motors => {
        bot.motors = motors
        return bot
      })
  }

  return getMotorsProps().then(createMotors)
}

// Create the Head
BotSchema.methods.addHead = (bot) => {
  return DB.Head
      ._create()
      .then((head) => {
        bot.head = head._id;
        log.info('Head sucessfully created !')
        return bot
      })
}

BotSchema.methods.addToolBase = (bot) => {
  return DB.ToolBase
      .create({})
      .then(toolBase => {
        bot.toolBase = toolBase._id;
        log.info('ToolBase sucessfully created !')
        return bot
      })
}

// Create the Scheduler
BotSchema.methods.addScheduler = function (bot) {
    return DB.Scheduler
      .create({})
      .then((scheduler) => {
          bot.scheduler = scheduler._id;
          log.info('Scheduler sucessfully created !')
          return bot
      })
}

// Create the Ground
BotSchema.methods.addGround = function (bot) {
  return new Promise((resolve, reject) => {
    return DB.Axis.find({
        name: {
          $in: ['x', 'y']
        }
      })
      .then(axis => {
        return DB.Ground.create({
          size: utils.CMtoHex(axis[0].size, axis[1].size)
        })
      })
      .then((ground) => {
        bot.ground = ground._id;
        return ground.addSlots(bot).then((ground) => {
          resolve(bot);
        });
      });
  });
}

BotSchema.methods.saveAndCreate = function (bot) {
  return new Promise((resolve, reject) => {
    return bot.save((err, _mdl) => {
      if (err) {
        log.err(err);
        reject(err);
        return;
      }

      log.debug('Bot sucessfully created !');

      resolve({
        success: true,
        msg: _mdl
      });
    });
  })
}

/* *** */

BotSchema.plugin(deepPopulate);

module.exports = {
  model: mongoose.model('Bot', BotSchema),
  schema: BotSchema
};
