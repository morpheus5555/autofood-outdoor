var mongoose = require('mongoose');
var deepPopulate = require('mongoose-deep-populate')(mongoose);

var MotorSchema = new mongoose.Schema({
	name: {
		type: String,
	},
	position: {
		type: Number,
		default: 0
	},
});

MotorSchema.statics.savePosition = (name,position) => 
    DB.Motor.updateOne({ name }, { position })
    .then((err,motor) => {
        log.success(`Position of ${name} has been saved at ${position}`)
        return motor
    })

MotorSchema.plugin(deepPopulate);
module.exports = { model: mongoose.model('Motor', MotorSchema), schema: MotorSchema };
