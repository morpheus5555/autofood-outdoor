var mongoose = require('mongoose');
var deepPopulate = require('mongoose-deep-populate')(mongoose);

var GroupSchema = new mongoose.Schema({
	/*_parent_id: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Group'
	},*/
	name: {
		type: String,
	},
	position: {
		type: Number,
		default: 0
	},
	endstops: {
		type: Array,
		default: Array(2).fill(false)
	},
	datas: {
		step_measure: [Number]
	},
}, {
	usePushEach: true
});

GroupSchema.methods.resetStepMeasure = function (axisName) {
	this.datas.step_measure = []

	return this.save() 
}

GroupSchema.plugin(deepPopulate);
module.exports = { model: mongoose.model('Group', GroupSchema), schema: GroupSchema };
