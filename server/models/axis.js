var mongoose = require('mongoose');
var deepPopulate = require('mongoose-deep-populate')(mongoose);


var AxisSchema = new mongoose.Schema({
	//bot_id: {}
	name: {
		type: String,
	},
	size: { // cm
		type: Number
	},
	sizeInStep: { // step
		type: Number
	},
	position: {
		type: Number,
		default: 0,
	},
	speed: {
		type: Number
	},
	acc: {
		type: Number
	},
	defaultSpeed: {
		type: Number,
		default: 250
	},
	defaultAcc: {
		type: Number,
		default: 200
	},
	
	groups: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Group'
	}],

}, {
	usePushEach: true
});

AxisSchema.virtual('stepPerCM').get(function() {
	return (this.sizeInStep / this.size).toFixed(2);
});

AxisSchema.static('getDirection', (axisName,target)  => new Promise((resolve,reject) => { 
	return DB.Axis
		.findOne({ name: axisName })
		.then((axis) => {
			if(axis.position === undefined) log.error(`Position is undefined in axis ${axis.name}`)

			// We determine the nearest endstop
			const nearest_end = axis.position > axis.sizeInStep / 2 ? 1 : 0
			
			if(!target) log.err('Incorrect Target Value');
			
			if(target == "near") {
				resolve(nearest_end);
			} else if(target == "far") {
				resolve(Number(!nearest_end));
			}
		})

}))

AxisSchema.methods.createGroups = function(props) {
	const groups = Array(props.n_group).fill().map((el, i) => ({
		name: this.name + i,
		endstops: Array(props.n_endstop_per_group).fill(false)
	}));

	return DB.Group.insertMany(groups)
		.then(groups => {
			groups.forEach(group => {
				this.groups.push(group._id);
			})

			return this.save();
		})
}

AxisSchema.static('getXYstepPerCM', () => {
	return Promise.all([
		DB.Axis.findOne({ name: 'x' }),
		DB.Axis.findOne({ name: 'y' })
	]).then((values) => {
		return { x: values[0].stepPerCM, y: values[1].stepPerCM }
	});
});

AxisSchema.static('updateSizeInStep', (axisName, stepMeasure) => {
	return DB.Axis
		.updateOne({ name: axisName }, { sizeInStep: Math.round(Math.abs(stepMeasure)) })
});

AxisSchema.static('resetGroupsStepMeasure', (axisName) => {
	return DB.Axis
		.findOne({ name: axisName })
		.populate('groups')
		.then(axis => Promise.all(axis.groups.map(group => {
			return group.resetStepMeasure();
		})))
});

AxisSchema.static('determineAngle', (chunk) => {
	//return getNumberOfchunk()
	//				.then()DB.Axis').findOne({ name: 'a' })
});

AxisSchema.plugin(deepPopulate);
AxisSchema.set('toObject', { virtuals: true });
AxisSchema.set('toJSON', { virtuals: true });
module.exports = { model: mongoose.model('Axis', AxisSchema), schema: AxisSchema };
