'use strict';

const deepPopulate = require('mongoose-deep-populate')(mongoose);
const fs = require('fs');
const initFile = __dirname + '/init/foodtype.json';

const FoodTypeSchema = new mongoose.Schema({
	name: String,
	radius: Number, // hexagon
	color: String, // #abcdef
	price: Number, // euros
	weight: Number,
	height: Number, // cm
	time: mongoose.Schema.Types.Mixed,
	water: mongoose.Schema.Types.Mixed,
	instances: { type: Number, default: 0 }, // Number of alive planted foods

	// snapshots: [{
	// 	type: mongoose.Schema.Types.ObjectID, ref: "FoodTypeSnapshot"
	// }],
});

const updateActionObjects = (done) => {
	const AO = require('./../actions/actionObjects');
	if(!AO.inited) return done();

	return AO.init().then(() => {
		require('./../socket').sendActions();
		done();
	});
}

FoodTypeSchema.pre('save', function(done) {
	this.wasNew = this.isNew; // used to know is the doc is new in post hook
	done();
})
FoodTypeSchema.post('save', function(doc, done) {
	if(!this.wasNew) return done();
	return updateActionObjects(done);
})
FoodTypeSchema.post('remove', function(doc, done) {
	return updateActionObjects(done);
})

FoodTypeSchema.static('createDefaults', () => new Promise((resolve,reject) => {
	log.info('Creating FoodTypes...');

	fs.readFile(initFile, 'utf8', (err, data) => {
		if(err) return reject(err);

		DB.FoodType
			.create(JSON.parse(data))
			.then((foodtypes) => {
				log.info('FoodTypes created!');
				resolve(foodtypes);
			})
	})
}));

FoodTypeSchema.static('getIds', () => new Promise((resolve) => {
	DB.FoodType
		.find()
		.exec((err,foodtypes) => {
			let ids = {}
			foodtypes.map((type) => {
				ids[type.name] = type._id;
			})
			resolve(ids);
		})
}));

/**
 * Increment the number of instances
 * @param {ObjectID} _id 
 */
FoodTypeSchema.statics.inc_instances = _id => DB.FoodType.updateOne({ _id }, { $inc: { instances: 1 } })
/**
 * Decrement the number of instances
 * @param {ObjectID} _id 
 */
FoodTypeSchema.statics.dec_instances = _id => DB.FoodType.updateOne({ _id }, { $dec: { instances: 1 } })
/**
 * Reset the number of instances
 * @param {ObjectID} _id 
 */
FoodTypeSchema.statics.reset_instances = _id => DB.FoodType.updateOne({ _id }, { instances: 0 })


FoodTypeSchema.plugin(deepPopulate);

module.exports = { model: mongoose.model('FoodType', FoodTypeSchema), schema: FoodTypeSchema };
