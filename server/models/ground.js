'use strict';

var mongoose = require('mongoose')
var deepPopulate = require('mongoose-deep-populate')(mongoose)
const utils = require('../actions/utils');
const async = require('async');
;

var GroundSchema = new mongoose.Schema({
	size: {
		x: Number,
		y: Number
	},
	slots: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Slots'
	}],
})

function createSlots(axis) {
	return new Promise((resolve,reject) => {
		if(axis.length === 0) reject('No Axis found');

		log.info('Creating Slots...');

		const dim = utils.CMtoHex(axis[0].size, axis[1].size);

		let slots = Array(dim.x * dim.y);

		return async.eachOf(slots, function(slot, index, callback) {
			const s = {
				position: {
					x: index % dim.x,
					y: Math.floor(index / dim.x)
				}
			}
			slots[index] = s;
			callback();
		},
		function (err) {
			if (err) log.err(err.message);
			// configs is now a map of JSON data
			//doSomethingWith(configs);
			let s = slots;
			resolve(slots);
		});
	});
}

function getAxis(_names) {
	return new Promise(function (resolve, reject) {
		DB.Axis.find({ 'name': { $in: _names} }, function(err, axis) {
			if(err)
				reject(err)
			else
				resolve(axis);
		});
	});
}

// Add slots at the creation of the bot
GroundSchema.methods.addSlots = function() {
	return new Promise((resolve,reject) => {
		return getAxis(['x','y'])
			.then(createSlots)
			.then((slots) => new Promise(_resolve => {
				DB.Slot
					.insertMany(slots)
					.then((new_slots,err) => {
						this.slots = new_slots;
						this.save(() => {
							log.info(`${new_slots.length} Slots Created !`);
							_resolve();
						});
					});
			}))
			.then(() => resolve(this))
		 /*	.then(() => {
				return DB.Chunk')
					.createAndInsertSlots({ x: this.size.x, y: this.size.y })
					.then(() => {
						resolve(this);
					})
					.catch(log.err)
			})*/
		// .catch... Must handle errors properly.
	});
}

let GroundModel = mongoose.model('Ground', GroundSchema)

module.exports = { model: GroundModel, schema: GroundSchema }

