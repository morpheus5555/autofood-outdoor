const mongoose = require('mongoose')
const deepPopulate = require('mongoose-deep-populate')(mongoose);

const ParameterSchema = new mongoose.Schema({
    name: { type: String },
    value: { type: mongoose.Schema.Types.Mixed }
});

ParameterSchema.plugin(deepPopulate);

module.exports = { model: mongoose.model('Parameter', ParameterSchema), schema: ParameterSchema };
