// @flow

var mongoose = require('mongoose');
var deepPopulate = require('mongoose-deep-populate')(mongoose);


const assert = require('assert')

const ToolModel = require('./tool').model

const SeedChunk = mongoose.model('SeedChunk', new mongoose.Schema({
    // Type of seed actually in the chunk
    type: { type: mongoose.Schema.Types.ObjectId, ref: 'FoodType' },

    // The index position of the chunk
    // 0 == The First after the endstops (Clockwise)
    index: Number,
}))


const SeederSchema = new mongoose.Schema({
    name: { type: String, default: "Seeder"},
    chunks: [{ type: mongoose.Schema.Types.ObjectId, ref: 'SeedChunk' }],
});

/**
 * This function is useful for rotating the A Axis.
 * 
 * With a given chunk size and it's index (position), 
 * Get the target absolute position of the center of the chunk
 * in steps
 * 
 * The size of a chunk (In steps)
 * @param {Number} size 
 * 
 * It's position (Example: The third one)
 * @param {Number} index 
 * 
 */
const chunkCenterPosition = (size, index) => size * index + size / 2

/**
 * For a given {@link FoodType} name,
 * Calculate the step to move the A Axis to the SeedChunk that contain
 * the right Seed
 * 
 * @param {String} foodtype_name 
 */
SeederSchema.statics.getStepForAngle = (foodtype_name) => 
    Promise.all([
        mongoose.model("Seeder").findType(foodtype_name),
        DB.Axis .findOne({ name: 'a' }),
        DB.Seeder.findOne(),
    ])
    .then(([chunk,axis,seeder]) => {
        return chunkCenterPosition(axis.sizeInStep / seeder.chunks.length, chunk.index)
    })
    
    /**
     * Find a chunk with a given foodtype
     * 
     * @param {FoodType} foodtype 
     */
    const findChunk = (foodtype) => {
        if(!foodtype) throw new Error('FoodType is undefined')
        else
        return DB.SeedChunk
        .findOne({ 'type': foodtype._id })
        .populate('type')
        .exec()
        .then(chunk => {
                assert(chunk !== undefined && chunk !== null, "Chunk not found")
            
                return chunk
            })
}

/**
 * Find a chunk by the name of foodtype it contain
 * on {@link SeedChunk#type}f
 * 
 * @param {String} foodtype_name
 */
SeederSchema.statics.findType = (foodtype_name) => 
    DB.FoodType
        .findOne({ 'name': foodtype_name })
        .lean()
        .then(findChunk)


/**
 * Check if the {@link SeedChunk}s actually have
 *  the requested seed/foodtype
 * 
 * @param {String} foodtype_name 
 */
SeederSchema.statics.checkSeeds = (foodtype_name) =>
    DB.Seeder
         .findType(foodtype_name)
        .then(chunk => {
            log.w(is.undefined(chunk), `SeedChunks does not have the FoodType (${foodtype_name})`)
            return chunk.type !== undefined && chunk.type !== null
        })


/**
 * 
 * @param {String} foodtype_name 
 */
SeederSchema.statics.getStepsByName = (foodtype_name) =>
    DB.Seeder
        .findType(foodtype_name)
        .then((chunk) => DB.Seeder.getStepForAngle(chunk.type.name))

/**
 * ==============  CREATION FUNCTIONS ================ 
 */

/**
 * Create the Default SeedChunk Array
 * at the DB creation
 */
const getDefaultChunks = () => {
    // Number of Chunks
    const n_chunks = 5
    const chunks = Array(n_chunks).fill({})

    return Promise.all([
        DB.FoodType.find(),
        Promise.map(chunks, (chunk, index) => chunk = { index })
    ]).then(([foodtypes, chunks]) => {
    
        foodtypes.slice(0,chunks.length).map((foodtype,i) => {
            chunks[i]["type"] = foodtype._id
        })
        return chunks
    })
}

/**
 * Create all the {@link SeedChunk} in the database
 * 
 * @param {Seeder} seeder 
 */
const createChunks = (seeder) => 
    getDefaultChunks()
        .then(chunks => 
            SeedChunk.create(chunks)
            .then(chunks => {
                seeder.chunks = chunks.map((chunk) => chunk._id)
                return seeder.save()
            })
        )


/**
 * Create the Seeder
 * And then its SeedChunks
 */
SeederSchema.statics._create = () =>
    mongoose.model('Seeder')
        .create({})
        .then(createChunks)

module.exports = {model: ToolModel.discriminator('Seeder', SeederSchema), schema: SeederSchema };
