module.exports = (board) => function (name, pin) {
	const gramsMultiplicator = 3;
	let pause = false;
	let readingPromise = Promise.resolve();

    const init = () => {
		board.pinMode(pin, board.MODES.ANALOG);
	}

	/**
	 * Read the pin just one time
	 * @returns {Promise} Promise which resolves new value
	 */
	const read = () => readContinuous(() => true);

	/**
	 * Read the pin until interrupt is true
	 * @param {Function} interrupt Function which when returns true stop the reading
	 * @returns {Promise} Promise which resolves new value when interrupt is true
	 */
	const readContinuous = (interrupt) => readingPromise = new Promise((resolve,reject) => {
		board.analogRead(pin, value => {
			value = toValueType(value);

			if(interrupt(value) === true) {
				board.reportAnalogPin(pin, 0);
				return resolve(value);
			}
		});
	})

	/**
	 * Give properties and methods (e.g. toGrams) to the value
	 * @param {number} value 
	 */
	const toValueType = (value) => Object.assign(value, {
		toGrams: () => toGrams(value)
	})

	const toGrams = (value) => {
		return value * gramsMultiplicator;
	}

	const stopReading = () => {
		pause = false;
		board.reportAnalogPin(pin, 0);
		readingPromise.cancel();
	}
	const pauseReading = () => {
		if(!pause) board.reportAnalogPin(pin, 0);
		pause = true;
	}
	const resumeReading = () => {
		if(pause) board.reportAnalogPin(pin, 1);
		pause = false;
	}

    return {
        init,
		name,
		read,
		toGrams,
		stopReading,
		pauseReading,
		resumeReading,
		read,
		readContinuous,
		getValue: () => toValueType(board.pins[board.analogPins[pin]].value)
    };
}

