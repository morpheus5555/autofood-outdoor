const async = require('async');
const Board = require("firmata");

const port = '/dev/ttyACM0';

let board = null;
const HardwareGroup = require("./hardwareGroup");
const components = {};

const createComponents = (board) => {
	const endstop = require("./endstop")(board);
	const group = require("./group")(board);
	const axis = require("./axis")(board);
	const motor = require("./motor")(board);
	const pump = require("./pump")(board);
	const loadCell = require("./loadCell")(board);

	const endstops = HardwareGroup([
		endstop("x1", 34, 0),
		endstop("x2", 35, 1),
		endstop("y11", 36, [0,1]),
		endstop("y12", 37, null),
		endstop("y13", 38, [0,1]),
		endstop("y21", 39, [0,1]),
		endstop("y22", 40, null),
		endstop("y23", 41, [0,1]),
		endstop("z11", 42, 0),
		endstop("z12", 43, 1),
		endstop("z21", 44, 0),
		endstop("z22", 45, 1),
		endstop("a", 46, [0,1])
	])
	const motors = HardwareGroup([
		motor("y1", 13, 12, 11, 0),
		motor("y2", 10, 9, 8, 1),
		motor("x1", 7, 6, 5, 2),
		motor("x2", 4, 3, 2, 3),
		motor("z1", 14, 15, 16, 4),
		motor("z2", 17, 18, 19, 5),
		motor("a", 22, 23, 24, 6),
		motor("b", 25, 26, 27, 7),
		motor("c", 28, 29, 30, 8),
		motor("d", 31, 32, 33, 9)
	])
	const pumps = HardwareGroup([
		pump("p1", 50),
		pump("p2", 51)
	])
	const loadCells = HardwareGroup([
		loadCell("c1", 52),
		loadCell("c2", 53)
	])

	const multi = function(num, motors) {
		const init = function() {
			board.multiStepperConfig({
				groupNum: num,
				devices: motors.map(motor => motor.config.deviceNum)
			})
		}

		return {
			init,
			num,
			motors,
		}
	}
	const multistepper_grps = {
		x: multi(0,	motors.getByNames(["x1", "x2"])),
		y: multi(1,	motors.getByNames(["y1","y2"])),
		z: multi(2, motors.getByNames(["z1","z2"])),
	}
	const axes = HardwareGroup([
		axis("x", [
			group(
				motors.getByNames(["x1", "x2"]),
				endstops.getByNames(["x1", "x2"])
		)],
		multistepper_grps.x
		),
		axis("y", [
			group(
				motors.getByNames(["y1"]),
				endstops.getByNames(["y11", "y12", "y13"]),
			),
			group(
				motors.getByNames(["y2"]),
				endstops.getByNames(["y21", "y22", "y23"]),
			)
		],multistepper_grps.y),
		axis("z", [
			group(
				motors.getByNames(["z1"]),
				endstops.getByNames(["z11", "z12"]),
			),
			group(
				motors.getByNames(["z2"]),
				endstops.getByNames(["z21", "z22"]),
			)
		], multistepper_grps.z),
		axis("a", [
			group(
				motors.getByNames(["a"]),
				endstops.getByNames(["a"]),
		)])
	])


	return {
		endstops,
		motors,
		axes,
		pumps,
		loadCells
	}
}

const initMessage = function() {
	log.info(`Firmware: ${board.name} ${board.firmware.name} v${board.firmware.version.major}.${board.firmware.version.minor}`);
	log.info(`On ${board.sp.path} at ${board.sp.baudRate} bps`);
	log.success("Controller is Ready");
}

const initComponents = () => {
	Object.assign(components, createComponents(board));

	Object.keys(components).forEach(componentName => {
		components[componentName].list.forEach(el => el.init())
	})
}

const init = () => new Promise((resolve, reject) => {
	log.info("Initialize Controller...");
	board = new Board(port, {}, (err) => {
		if(!err) return;
		log.err(err);
		reject(err);
	});

	board.on("ready", () => {
		// Arduino is ready to communicate
		initMessage();
		initComponents();

		setTimeout(() => {
			log.info("Hardware is fully initialized");
			resolve();
		}, 800);
	});
})


module.exports =  {
	components,
	init
};
