const AxisModel = require('mongoose').model('Axis');
const async = require('async')

module.exports = (board) => function(name, groups, stepper_group) {
    const isSingle = !stepper_group && groups.length === 1 && groups[0].motors.length === 1;
    let position = 0;
    let stopped = false;
    let paused = false; // has the current move been paused at least once ?
    let onpause = false; // am i waiting to resume the current move ?
    let lastMoveArgs = null;
    const isInverted = () => P[name.toUpperCase() + '_INVERT'];

    const init = () => {
        if(stepper_group !== undefined)
            stepper_group.init()

        groups.forEach((group, i) => {
            group.init(i, name);
        })

        AxisModel.findOne({ name }).then((axis) => {
            position = axis.position;
            forEach('motors', motor => {
                motor.setPos(position);
            })
        })
    }

    // A shortcut for applying a callback
    // to Each element in Each Groups
    const forEach = (n, fn, c) => {
        return async.each(groups, (group,callback) => {
            return async.each(group[n], (obj, cb) => {
                fn(obj, cb);
            }, (err) => {
                if(err) log.err(err);
                callback();
            })
        }, (err) => {
            if(err) log.err(err);
            if(c !== undefined)
                c();
        });
    }

    const onMoveEnd = (force = false) => {
        // return here because it's manually called when paused or stopped
        if(!force && (onpause || stopped)) return Promise.resolve();

        const motors = groups.reduce((a,c) => {
            a.push(...c.motors);
            return a;
        }, []);

        const afterMove = () => {
            position = getMeanMotorPos();
            resetPos();

            if(!onpause) {
                if(!stopped) require('../actions/actionManager').receive('end')
                paused = false;
            }

            stopped = false;

            return Promise.resolve();
        }

        return motors.length <= 1 ? afterMove() : 
            Promise.all(motors.map(motor => new Promise(resolve => {
                motor.onEnd(resolve, isInverted())()
            }))).then(afterMove)
    }

    const resetPos = () => {
        return forEach('motors', (motor) => {
            motor.resetPos();
        }) 
    }

    const moveTo = (args) => {
        lastMoveArgs = args;

        const relativePos = isInverted() ? position-args.pos : args.pos-position;
        // const relativePos = args.pos - position;
        const dir = args.pos - position > 0 ? 1 : 0;

        const listGroupsLimit = groups.map(group => group.isOnLimit(dir, position));

        // does ALL groups can't go any further in that direction ?
        const areAllGroupsOnLimit = listGroupsLimit.reduce((a, c) => a ? c : false, true)
        // does ALL groups are free to move in that direction ?
        const areAllGroupsNotOnLimit = listGroupsLimit.reduce((a, c) => a ? !c : false, true)

        forEach('motors', (motor) => {
            if(motor.getSpeed() !== args.speed)
                motor.setSpeed(args.speed);
            if(motor.getAcc() !== args.acc)    
                motor.setAcc(args.acc);
        })

        if(areAllGroupsOnLimit) { // end if there is nothing to move
            require('../actions/actionManager').receive('end')
        } else {
            if(areAllGroupsNotOnLimit) { // move all motors
                if(isSingle)
                    groups[0].motors[0].moveTo(relativePos, onMoveEnd);
                else
                    board.multiStepperTo(stepper_group.num, [relativePos], onMoveEnd)
            } else {
                groups.forEach((group, i) => { // move only motors whose group is free to move
                    // group.motors[0] works here because there is no axis with multiple group
                    // and more than one motor per group so, may change it later
                    // but I don't care right now
                    if (!listGroupsLimit[i]) group.motors[0].moveTo(relativePos, onMoveEnd)
                });
            }
        }
    }

    const stopMotors = () => {
        if(isSingle)
            groups[0].motors[0].stop();
        else
            board.multiStepperStop(stepper_group.num);

        return onMoveEnd(true)
    }

    const stop = () => {
        stopped = true;
        onpause = false;

        return stopMotors();
    }

    const pause = () => {
        if(onpause) return Promise.resolve();

        if(!paused) { // reset relative pos on first pause
            forEach('motors', motor => {
                motor.setRelativePos(0);
                motor.pause()
            })
        }

        onpause = true;
        paused = true;

        return stopMotors();
    }

    const resume = () => {
        if(!onpause || lastMoveArgs === null) return;

        forEach('motors', motor => {
            motor.resume()
        })
        onpause = false;

        moveTo(lastMoveArgs);
    }

    const stopGroupsOnLimit = (dir, pos) => {
        groups.forEach((group) => {
            if(group.isOnLimit(dir, pos)) group.stop();
        })
    }

    const getAllMotorPos = (relative = false) => {
        return groups.reduce((acc, group) => {
            group.motors.map(motor => {
                if(relative)
                    acc.push(motor.getRelativePos())
                else 
                    acc.push(motor.getPos())
            })

            return acc;
        }, [])
    }

    const getMeanMotorPos = (relative = false) => {
        return Math.round(_.mean(getAllMotorPos(relative)))
    }

    return {
        init,
        name,
        groups,
        forEach,
        getPos: () => position,
        setPos: (pos) => position = pos,
        resetPos,
        moveTo,
        stop,
        pause,
        resume,
        stopGroupsOnLimit,
        getAllMotorPos,
        getMeanMotorPos,
        areAllGroupsLimitEndstopsPushed: (dir) => {
            return groups.reduce((a,group) => a ? group.areAllLimitEndstopsPushed(dir) : false, true);
        },

        get defaultSpeed() { return getDefaultSpeed() },
        get defaultAcc() { return getDefaultAcc() },
        get inverted() { return isInverted() },
    }
}
