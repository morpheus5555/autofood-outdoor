module.exports = (board) => function (name, pin) {
    const init = () => {
		board.pinMode(pin, board.MODES.OUTPUT);
	}

	const on = () => new Promise(resolve => {
		board.digitalWrite(pin, board.HIGH);
		resolve();
	})

	const off = () => new Promise(resolve => {
		board.digitalWrite(pin, board.LOW);
		resolve();
	})

    return {
        init,
		name,
		
		// CONTROLLER
		on, off,
		flow: (volume, delay) => DB.Pump
			.volumeToSec({ name, volume })
			.then(sec => on()
				.delay(sec*1000)
				.then(off)
				.delay(delay)
			)
    };
}

