module.exports = (board) => function (name, pin, pushedOnLimit, behavior = () => {}, motors=[]) {
	// (null -> []) and (0 -> [0]) and ([0] -> [0])
	pushedOnLimit = pushedOnLimit === null ? [] : !Array.isArray(pushedOnLimit) ? [pushedOnLimit] : pushedOnLimit;

	let faking = false; // If true, act like being pushed
	let notifyPush = () => {}; // Send push event to group
	let notifyPull = () => {}; // Same with pull
	let lastValue = board.LOW;
	let lastChangeTime = 0;
	const minTimeDiff = 5 // millisec, minimum time between push

	const init = () => {
		board.pinMode(pin, board.MODES.INPUT);
		board.digitalRead(pin, (value) => {
			const now = Date.now();
			const timeDiff = now - lastChangeTime;
			if(timeDiff > minTimeDiff) {
				if(value === board.HIGH && lastValue === board.LOW) {
					onPushed();
				}
				else if(value === board.LOW && lastValue === board.HIGH) {
					onPulled();
				}
			}

			if(value !== lastValue) {
				lastValue = value;
				lastChangeTime = now;
			}
		});
	}

	/**
	 * Define functions called to notify group of push/pull
	 * @param {Function} fnPush 
	 * @param {Function} fnPull 
	 */
	const setNotifiers = (fnPush, fnPull) => {
		notifyPush = fnPush;
		notifyPull = fnPull;
	}

	const stopMotors = () => {
		motors.forEach(motor => {
			motor.stop();
		});
	}

	const onPushed = () => {
		notifyPush(name, pushedOnLimit, stopMotors);
		// behavior();
	}

	const onPulled = () => {
		notifyPull(name, pushedOnLimit, stopMotors);
	}

	const fakePush = () => {
		faking = true;
		onPushed();
	}
	const unfake = () => {
		faking = false;
		onPulled();
	}

	return {
		init,
		name,
		pin,
		motors,
		isPushedOnLimit: (limit) => pushedOnLimit.indexOf(limit) !== -1,
		onPushed,
		stopMotors,
		setMotors: _motors => motors = _motors,
		isPushed: () => faking || Boolean(board.pins[pin].value),
		fakePush,
		unfake,
		setNotifiers
	}
}
