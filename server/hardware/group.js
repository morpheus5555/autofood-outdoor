module.exports = (board) => function(motors, endstops) {
    endstops.forEach(endstop => {
        endstop.setMotors(motors)
    });

    let index = null;
    let axisName = null;

    const init = (_index, _axisName) => {
        index = _index;
        axisName = _axisName;
            
        motors.forEach(motor => {
            motor.groupIndex = index;
        });
        endstops.forEach(endstop => {
            endstop.setNotifiers(endstopPushEvent, endstopPullEvent);
        });
    }

    const socketEndstopUpdate = (endstopName, value) => {
		Socket.endstopUpdate(axisName, index, endstops.findIndex(e => e.name === endstopName), value);
    }
    const endstopPushEvent = (endstopName, pushedOnLimit, stopMotors) => {
        // When a endstop is pushed, stop motors only if all endstops which
        // define the same limit(s) are also pushed
        // (currently only Y and A got more than 1 limit per endstop)
        pushedOnLimit.forEach(dir => {
            if(areAllLimitEndstopsPushed(dir)) {
                stopMotors();
            }
        })

		socketEndstopUpdate(endstopName, true);
    }
    const endstopPullEvent = (endstopName, pushedOnLimit, stopMotors) => {
		socketEndstopUpdate(endstopName, false);
    }

    const areAllLimitEndstopsPushed = (dir) => {
        return endstops.reduce((a,c) => a ? !(!c.isPushed() && c.isPushedOnLimit(dir)) : false, true)
    }

    return {
        init,
        index,
        motors,
        endstops,
        stop: () => endstops[0].stopMotors(),
        areAllLimitEndstopsPushed,
        isOnLimit: (dir, position) => {
			const checkEndstop = endstop => {
                if(endstop.isPushedOnLimit(dir)) {
					if(!endstop.isPushed()) return false;

					if(dir === 0 && position > 0) {
						return false;
					} else if(dir === 1 && position === 0) {
						return false;
					}
                }
                
				return true;
			}

            return endstops.reduce((a, c) => a ? checkEndstop(c) : false, true);
        }
    }
}