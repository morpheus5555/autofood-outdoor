module.exports = function(list) {
    let obj = {};
    list.forEach((o) => {
        if(o.name) obj[o.name] = o;
    });

    const G ={
        list,
        getByName: (name) => list.find((el) => el.name === name),
        getByNames(names) {
            return names.map(name => this.getByName(name));
        },
        // Get an array of pins of the list
        // pins: () => Promise.reduce(list, (pins, hardware,i) => { pins.push(hardware.pin); return pins; }, [])
    }

    return Object.assign(G, obj)
}
