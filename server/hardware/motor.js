module.exports = (board) => function (name, pin_pul, pin_dir, pin_ena, dnum, speed = 200, acc = 200) {

const config = {
    deviceNum: dnum,
    type: board.STEPPER.TYPE.DRIVER,
    stepSize: board.STEPPER.STEP_SIZE.WHOLE,
    stepPin: pin_pul,
    directionPin: pin_dir,
    enablePin: pin_ena,
    invertPins: 0
}

let groupIndex = null;
let axisName = name[0]
let pos = 0;
let relativePos = 0;
let paused = false

const setPos = (_pos) => pos = _pos
const getPos = () => pos
const setRelativePos = (_rpos) => relativePos = _rpos
const getRelativePos = () => relativePos
const getSpeed = () => speed
const getAcc = () => acc

const init = () => {
    board.accelStepperConfig(config);
    board.accelStepperSpeed(dnum, speed);
    board.accelStepperAcceleration(dnum, acc);
}
    
const onEnd = (callback, inverted = false) => () => {
    board.accelStepperReportPosition(config.deviceNum, function(value) {
        value = inverted ? -value : value;
        const relativePos = paused ? getRelativePos() + value : value

        // Update the motor position value
        setPos(pos + value);
        setRelativePos(relativePos);

        // And notify current action with it
        if(!paused)
            require('../actions/actionManager').receive('datas', {
                data: relativePos,
                axisName: axisName,
                groupIndex: groupIndex,
                motorName: name
            })

        callback()
    })
}


return {
    init,
    name,
    axisName,
    groupIndex,
    config,
    setPos,
    getPos,
    setRelativePos,
    getRelativePos,
    getSpeed,
    getAcc,

    pause: () => paused = true,
    resume: () => paused = false,

    onEnd,

    // CONTROLLER
    setSpeed: (_speed) => { speed = _speed; board.accelStepperSpeed(dnum, _speed); },
    setAcc: (_acc) => { acc = _acc; board.accelStepperAcceleration(dnum, _acc); },
    enable: (_ena) => { board.accelStepperEnable(dnum, ena); },
    stop: () => { board.accelStepperStop(dnum); },
    moveTo: (_pos, callback) => { 
        board.accelStepperTo(dnum, _pos, onEnd(callback));
    },
    move: (_steps, callback) => {
        board.accelStepperStep(dnum, _steps, onEnd(callback)); 
    },
    resetPos: () => { board.accelStepperZero(dnum) },
    // getPos: (callback) => board.accelStepperReportPosition(dnum,callback)
};

}