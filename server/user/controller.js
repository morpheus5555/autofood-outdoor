var LocalStrategy   = require('passport-local').Strategy;
var User = require('./../models/user');
var msg = require('./../config/msg');

// expose this function to our app using module.exports
module.exports = function(passport) {
  var email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });

  passport.use('local-login', new LocalStrategy({
    usernameField : 'email',
    passwordField : 'password'
  },
  function(email, password, done) {
    process.nextTick(function () {
      User.findOne({ 'email': email }, function (err, user) {
        if (err) return done(err);
        if (!user) return done(null, false, msg.err.user_login_email);
        if (!user.validPassword(password)) return done(null, false, msg.err.user_login_password);

        done(null, user);
      });
    });
  }));

  passport.use('local-signup', new LocalStrategy({
    usernameField : 'email',
    passwordField : 'password'
  },
  function(email, password, done) {
    process.nextTick(function () {
      if (!email_regex.test(email)) return done(msg.err.user_signup_email);
      if (password.length < 6 || password.length > 20) return done(msg.err.user_signup_password_length);

      User.findOne({ 'email': email }, function(err, user) {
        if (err) return done(err);
        if (user) return done(null, false, msg.err.user_signup_email_taken);

        var newUser = new User();

        newUser.email = email;
        newUser.password = newUser.generateHash(password);

        newUser.save(function(err) {
          if (err) return done(err);

          done(null, newUser);
        });
      });
    });
  }));
};
