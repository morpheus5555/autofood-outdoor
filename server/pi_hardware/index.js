const cam = require('./cam')

module.exports = {
    cam,
    init: () => {
        cam.init()
    }
}