const CAM = require('camerapi');

/**
 * Check if the current HOSTNAME of this linux machine is valid,
 * e.g has the form "autofood-[id]"
 * 
 * Useful for checcking if we currently run on a Raspberry PI machine or 
 * a test computer
 * 
 * @return {Boolean}
 */
function validHostname() {
    const hostname = require('os').hostname()
    return hostname.startsWith('autofood-')
}

function checkHostname() {
    if(!validHostname()) {
        log.warn("Cannot do this  because of invalid HOSTNAME")
        return false;
    }
    return true;
}

/**
 * The basic function for capturing
 * 
 * @param {Function} callback 
 */
function capture(callback) {
    if(!checkHostname()) return;

    CAM.prepare({
        "timeout" : 0, 
        "width" : 1920,
        "height" : 1080,
        "quality" : 85

    }).takePicture('/img', callback);
}

/**
 * The basic function for recording
 * 
 * @param {Function} callback 
 */
function record(callback) {
    if(!checkHostname()) return;

    CAM.timeout(0)
    .bitrate(3500000)
    .framerate(5)
    .fullscreen()
    .recordVideo("myvideo.h264",callback);
}

function retrieve() {
    if(!checkHostname()) return;

    capture((file,error) => {
        
        console.log(file)
    })
}

function init() {
    if(validHostname()) {

        log.success("Camera initialized")
    } else {
        log.debug(`Camera not initialized, Invalid HOSTNAME (${ require('os').hostname() }) `)
    }
}

module.exports  = {
    init,
    capture,
}