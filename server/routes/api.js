'use strict';


const getBot = require('../getBot')


module.exports = function (app) {
  var apis = {
    foods: require('./../api/foods'),
    bots: require('./../api/bots'),
    shelves: require('./../api/shelves'),
    food_types: require('./../api/food_types'),
    groups: require('./../api/groups'),
  }


  app.use('/api/bots/:bot_id/foods', apis.foods);
  app.use('/api/bots/:bot_id/shelves', apis.shelves);
  app.use('/api/bots', apis.bots);

  app.get('/api/axes/:axis/reset_step_measure', (req, res) => {
		DB.Axis
			.resetGroupsStepMeasure(req.params.axis)
			.then(() => res.sendStatus(200))
	});  

  app.post('/api/foodtypes', (req, res) => {
    getBot(bot => {
      bot.addFoodType(req.body)
        .then(foodType => {
          res.send(foodType)
        })
    })
  });  

  app.put('/api/foodtypes/:id', (req, res) => {
    DB.FoodType.updateOne({
      _id: req.params.id
    }, { $set: req.body }).then(foodType => {
      res.send(foodType)
    })
  });

  app.delete('/api/foodtypes/:id', (req, res) => {
    // Can delete foodtype only if it's not used
    DB.Food.isTypeUsed(req.params.id)
      .then(isTypeUsed => {
        if(isTypeUsed) return res.status(500)
          .send(`This FoodType is used, so it cannot be removed`);
        else {
          getBot(bot => 
            bot.deleteFoodType(req.params.id)
              .then(() => {
                res.sendStatus(200);
              })
          )
        }
      })
  });

  app.put('/api/parameters', (req, res) => {
    require('../parameters').setParameters(req.body).then(() => {
      res.sendStatus(200);
    }).catch(err => {
      res.status(500).send(err);
    })
  });
 
  app.post('/api/scheduleritem', (req,res) => {
    let item = null;
    DB.SchedulerItem
      .create(req.body)
      .then(_item => {
        item = _item;
        return item;
      })
      .then(DB.Scheduler.addItem)
      .then(() => {
        res.send(item);
      })
      .catch((err) => {
        log.onerr(err);
        res.sendStatus(500);
      })
  })

};
