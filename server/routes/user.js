'use strict';

var mongoose = require('mongoose');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var User = mongoose.model('User');
var msg = require('./../config/msg');
require('./../user/controller')(passport);

module.exports = function (app) {
  app.post('/user/login', isNotLoggedIn, function(req, res, next) {
    passport.authenticate('local-login', {
      badRequestMessage: msg.err.user_empty_inputs
    }, function(err, user, info) {
      if (err) return res.status(500).json({ success: false, msg: err });
      if (!user) return res.status(404).json({ success: false, msg: typeof info === 'object' ? info.message : info });

      req.logIn(user, function(err) {
        if (err) return res.status(500).json({ success: false, msg: err });

        user.password = undefined; // remove encrypted password (because it's useless)

        res.json({ success: true, msg: msg.win.user_logged_in });
      });
    })(req, res, next);
  });

  app.get('/user/logout', isLoggedIn, function(req, res) {
    req.logout();
    res.redirect('/');
  });

  app.post('/user/register', isNotLoggedIn, function(req, res, next) {
    passport.authenticate('local-signup', {
      badRequestMessage: msg.err.user_empty_inputs
    }, function(err, user, info) {
      if (err) return res.status(500).json({ success: false, msg: err });
      if (!user) return res.status(404).json({ success: false, msg: typeof info === 'object' ? info.message : info });

      res.json({ success: true, msg: msg.win.user_signed_up });
    })(req, res, next);
  });

  app.get('/user/me', isLoggedIn, function(req, res, next) {
    var user = req.user;
    user.password = undefined;

    res.json({ success: true, user: user });
  });

  function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) return next();

    res.status(500).json({ success: false, msg: msg.err.user_not_logged_in });
  }

  function isNotLoggedIn(req, res, next) {
    if (!req.isAuthenticated()) return next();

    res.status(500).json({ success: false, msg: msg.err.user_logged_in });
  }
};
