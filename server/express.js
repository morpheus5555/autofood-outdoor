const path = require('path');
const express = require('express');
const config = require('./config/environment');
const fs = require('fs');
const cors = require('cors');
;
;

const app = express();
app.use(cors());

require('./config/express')(app);

fs.readdirSync(path.join(__dirname, 'models')).forEach(function (file) {
	if (~file.indexOf('.js')) require(path.join(__dirname, 'models', file));
});
fs.readdirSync(path.join(__dirname, 'routes')).forEach(function (file) {
	if (~file.indexOf('.js')) require(path.join(__dirname, 'routes', file))(app);
});

app.get('/', function(req, res) {
	res.send('Bot is active');
});

app.use(function (req, res) {
	var stream404 = fs.createReadStream(path.join(config.root, "static", "404.html"));
	stream404.pipe(res);
});
app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	res.header("Access-Control-Allow-Headers", "Content-Type");
	res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
	next();
});
	  

module.exports = {
	app,
	start: () => new Promise((resolve) => {
		const server = require('http').createServer(app);

		server.listen(config.port, config.ip, function () {
			log.info(`Express server listening on ${config.port}, in ${app.get('env')} mode`);
			resolve();
		});
	})
}