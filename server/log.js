// A little library that handle console colors
const colors = require('colors/safe');

/**
 * A more fancy and practical way to log message,
 * Available through all the program.
 * 
 * @param {String} type 
 * 
 * For console/cli use only:
 * Just a string prefix
 * @param {String} prefix 
 * 
 * For front-end use only: The font-awesome logo string
 * Use it like this: "fa fa-[logo]"
 * @param {String} logo 
 * 
 * Actual String that contain the message
 * @param {String} message 
 * 
 * Level of importance of the log
 * @param {Number} level 
 * 
 * For front-end use only: the hexadecimal color
 * @param {String} color 
 * 
 * For console/cli: the string color
 * @param {String} c_color 
 */
const log = (type, prefix, logo,  message, level, color, c_color) => {
    if(message === undefined || message === null)
        return;

    console[type](colors[c_color](`${prefix} ${message}`))

    if(level > 0)
        require('./socket').cmdAll('log_add', {type, prefix, logo, message, level, color})
}


exports.err = (message) => {
    log('error', '(( ! )) ', 'exclamation', message, 3, "#dc4b4b", "red")
    return Error(message)
}
exports.warn = (message, level=1) => log('warn', '// ! \\ ', 'exclamation-triangle', message, 2, "#dcc64b", "yellow")
exports.debug = (message, level=1) => log('debug','>> ', 'bug', message, 1, "#669ed6", "blue")
exports.info = (message, level=1) => log('info', '(( i )) ', 'info-circle', message, level, "#aaa", "blue")
exports.success = (message, level=1) => log('info', '✓', 'check', message, 2, "#00bb55", "bgGreen")

const types = {
    'e': exports.err,
    'w': exports.warn,
    'd': exports.debug,
    'i': exports.info,
    's': exports.success,
}

/**
 * Wrap a log with a condition
 * 
 * @param {String} type 
 * @param {Boolean} cond 
 * @param {String} message 
 */
exports.assert = (cond, type, message, callback, level = 1) => {

    if(cond)
        types[type](message, level)

    if(callback)
        return callback()
}

exports.e = (cond, message, callback,level = 1) => exports.assert(cond,'e',message,callback,level)
exports.w = (cond, message, callback, level = 1) => exports.assert(cond,'w',message,callback,level)
exports.d = (cond, message, callback, level = 1) => exports.assert(cond,'d',message,callback,level)
exports.i = (cond, message, callback, level = 1) => exports.assert(cond,'i',message,callback,level)
exports.s = (cond, message, callback, level = 1) => exports.assert(cond,'s',message,callback,level)

global.M = {
    BLOWER_WRONG_TOOL: () => exports.warn("Cannot move blower,  Head has not the right tool"),
    MOVING: (subject,position) => 	exports.debug(`Moving ${subject} to ${position}...`),
}