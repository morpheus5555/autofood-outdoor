const BotModel = require('./models/bot').model;

module.exports = function(cb) {
	BotModel.find({}, (err, b) => {
		if(err) log.err(err);
		return cb(b[0])
	});
}
