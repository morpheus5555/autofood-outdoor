const config = require('./config/environment');
const WebSocket = require('ws');

let wss = null;





function init() {
	return new Promise((resolve, reject) => {
		wss = new WebSocket.Server({
			port: config.socket_port || 7777
		});

		wss.on('connection', onconnection);

		resolve();
	})
}

function broadcast(data) {
	if(!wss) return;

	wss.clients.forEach(function each(client) {
		if (client.readyState === WebSocket.OPEN) {
			client.send(data);
		}
	});
}

function onconnection(ws) {
	// send the list of actionobjects (events and actions)
	// ws.send(command("events", require('./actions/actionObjects').argumentsDefinition));

	ws.send(getActionsDefinition('events'));
	ws.send(getActionsDefinition('actions'));
	ws.send(command('parameters', P));

	// begin to listen to incoming messages
	ws.on('message', onmess(ws));

	log.success("Connected to " + require('os').hostname());
}

const command = (type, data) => {
	return JSON.stringify({ type, data, timestamp: Date.now() });
}

function onmess(ws) {
	return function(message) {
		log.info(`FROM SOCKET > ${message}`, 0);

		message = JSON.parse(message);
		if(message.type === "get_hardware_state") {
			ws.send(command("hardware", {
					endstops: require('./hardware').components.endstops.list
				}))
		} else if(message.type === "get_queue") {
			sendQueue();
		} else if(message.type === "queue_delete_id") {
			require('./actions/actionManager').deleteAction(message.data.id);
		} else if(message.type === "get_scheduler") {
				ws.send(command("scheduler_items", { items: require('./scheduler').items }))
		} else if(message.type === "get_capture") {
			
			require('./pi_hardware/cam').capture((file,error) => {

				ws.send(command("last_capture", { file }))
			})
		} else {
			incoming(message)
		}
	}
}

function cmdAll(type, data) {
	broadcast(command(type, data))
}
function endstopUpdate(axis, grpIndex, index, value) {
	cmdAll('endstop_update', { axis, index, grpIndex, value })
}
function sendQueue() {
	cmdAll("queue", require('./actions/actionManager').QueueFormatter.JSON())
}
function deleteQueueItem(id) {
	cmdAll("queue_delete_item", {
		"eee": {"ee": 555}, id
	})
}
const sendAxis = (name,axis) => cmdAll("set_axis",{ name, axis })
const seedFood = (food, position) => cmdAll("seed_food", {food, position})

function getActionsDefinition(type) {
	return command(type, require('./actions/actionObjects').definition[type]);
}
function sendActions() {
	broadcast(getActionsDefinition('actions'))
	broadcast(getActionsDefinition('events'))
}

function incoming(message) {
	let parsed;
	try {
		parsed = JSON.parse(message.data);
	} catch(e) {
		log.err('Invalid JSON');
		return;	
	}
	require('./actions/actionManager').incomingAction(parsed);
}

module.exports = {
	init,
	broadcast,
	cmdAll,
	sendQueue,
	deleteQueueItem,
	sendAxis,
	sendActions,
	seedFood,
	endstopUpdate
}

/*
var noble = require('noble');
console.log(noble);

noble.on('stateChange', function(state) { 
	noble.startScanning();
	console.log(state);

//noble.discoverCharacteristics();
	//noble.discoverServices()
//noble.connect()


});



noble.on('discover', (p) => {
	console.log(p)
})

noble.on('characteristicsDiscover', function(p_uuid, s_uuid, characs) { 
	console.log(p_uuid, s_uuid, characs);
	noble.startScanning();
	noble.onCharacteristicsDiscover((p_uuid, s_uuid, characs) => {
		console.log(p_uuid, s_uuid, characs);
	})
	console.log(state);
});
*/



// const bleno = require('bleno')

// function AutofoodService() {
// 	bleno.PrimaryService.call(this, {
// 		uuid: '',
// 		characteristics: [

// 		]
// 	})
// }

// bleno.on('stateChange', function(state) {
// 	console.log('on -> stateChange: ' + state);
  
// 	if (state === 'poweredOn') {
// 	  bleno.startAdvertising('echo', ['ec00']);
// 	} else {
// 	  bleno.stopAdvertising();
// 	}
//   });
  
//   bleno.on('advertisingStart', function(error) {
// 	console.log('on -> advertisingStart: ' + (error ? 'error ' + error : 'success'));
  
// 	if (!error) {
// 	  bleno.setServices([
// 		new BlenoPrimaryService({
// 		  uuid: 'ec00',
// 		  characteristics: [
// 			new EchoCharacteristic()
// 		  ]
// 		})
// 	  ]);
// 	}
//   });

/*

function command(type, data) {
	return JSON.stringify({ type, data });
}
exports.cmdAll = function(type, data) {
	exports.broadcast(command(type,data))
}

const actionManager = require('./actions/actionManager');

exports.wss.on('connection', function(ws) {
	// Send the list of ActionObjects (Events and Actions)
	ws.send(command("events", require('./actions/actionObjects').getEventsWithDefaults()));
	ws.send(command("actions", require('./actions/actionObjects').getActionsWithDefaults()));

	// Begin to listen to incoming messages
	ws.on('message', onmess(ws));

	log.debug("Connected");
});

function onmess(ws) {
	return function(message) {
		message = JSON.parse(message);
		log.info(`FROM SOCKET > ${message}`, 0);
		
		if(message.type === "get_hardware_state") {
			ws.send(command("hardware", {
					endstops: require('./hardware').endstops.list
				}))
		} else if(message.type === "get_queue") {
			exports.sendQueue();
		} else if(message.type === "queue_delete_id") {
			require('./actions/actionManager').deleteAction(message.data.id);
		} else if(message.type === "get_scheduler") {
				ws.send(command("scheduler_items", { items: require('./scheduler').items }))
		} else {
			incoming(message)
		}
	}
}

exports.sendQueue = () => {
	exports.cmdAll("queue", require('./actions/actionManager').QueueFormatter.JSON())
}
exports.deleteQueueItem = (id) => {
	exports.cmdAll("queue_delete_item", {
		"eee": {"ee": 555}, id
	})
}

function incoming(message) {
	let parsed;
	try {
		parsed = JSON.parse(message.data);
	} catch(e) {
		log.err('Invalid JSON');
		return;	
	}
	require('./actions/actionManager').incomingAction(parsed);
}
*/