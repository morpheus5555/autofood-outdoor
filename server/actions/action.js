module.exports =  function Action(fn) {
	const Interface =  {
		HW: require('../hardware').components,
		end: () => require('./actionManager').receive('end'),
		stop: () => require('./actionManager').receive('stop'),
	}

	return fn(Interface);
}
