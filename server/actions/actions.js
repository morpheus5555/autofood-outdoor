const basics = require('./lib/basics')
const debugs = require('./lib/debugs')
const head = require('./lib/head')
const toolbase = require('./lib/toolBase')
const seeds = require('./lib/seeds')
const harvests = require('./lib/harvests')
const waters = require('./lib/waters')
const maintenances = require('./lib/maintenances')
const calibrations = require('./lib/calibrations')

const aO = require('./actionObjects').actions

function ActionList() {
	const list = Object.assign({},
		basics,
		debugs,
		head,
		toolbase,
		seeds,
		harvests,
		waters,
		maintenances,
		calibrations);

	//function add(name, exec) {
		//list[name] = exec;
	//}

	function get(name) {
		return list[name];
	}

	//function addMany(obj) {
		//Object.keys(obj)
			//.map(name => {
				//add(name, obj[name]);
			//});
	//}

	function exist(name) {
		if (list[name] === undefined) return false;
		else return true;
	}

	// function getActionObject(name, args) {
	// 	if(!exist(name))
	// 		return log.err(`Action ${name} doesn't exist`)
		
	// 	return aO.actionObject(name, args);
	// }

	return {
		list,
		//add,
		get,
		//addMany,
		exist,
	} 
}

module.exports = ActionList()
