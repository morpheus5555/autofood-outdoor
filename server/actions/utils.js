
const AO = require('./actionObjects').actions

const hexagonDiameter = 2; // cm	

const hexagonSize = {
	x: Math.sqrt(3) / 2 * hexagonDiameter,
	y: hexagonDiameter
}

const hexagonDistance = {
	x: hexagonSize.x,
	y: 3/4 * hexagonSize.y 
}


const YOffset = (toolbase_size, stepPerCM, secure_offset = 4000) => {
	return (toolbase_size *stepPerCM) + secure_offset
}

module.exports = {
	hexagonToStep(x, y) {
		return Promise.all([
			DB.Axis.getXYstepPerCM(),
			DB.ToolBase.findOne()
		]).then(([stepPerCM, toolbase]) => {
			
			
			const isYeven = y % 2 == 0;
			
			const pos = {
				x: x * hexagonDistance.x + (isYeven ? hexagonSize.x / 2 : hexagonSize.x),
				y: y * hexagonDistance.y + hexagonSize.y / 2
			}


			const secure_offset = 4000


			return {
				x: Math.round(pos.x * stepPerCM.x),
				y: Math.round(pos.y * stepPerCM.y)  + YOffset(toolbase.size.y,stepPerCM.y)
			}
		})
	},

	CMtoHex(x_cm, y_cm) {
		return {
			x: Math.floor(x_cm / hexagonDistance.x),
			y: Math.floor(y_cm / hexagonDistance.y)
		}
	},

	checkTool(tool_name) {
		return DB.Head
			.hasTool(tool_name)
			.then((hasTool) => {
				//if(!hasTool)
					return hasTool 
			})
	}
}
