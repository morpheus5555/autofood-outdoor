const Action = require('../action')
const utils = require('../utils');
const getBot = require('../../getBot')

const BotModel = require('mongoose').model('Bot');
const AxisModel = require('../../models/axis').model // require('mongoose').model('Axis');
const AO = require('../actionObjects').actions


const mean = (arr) => arr.reduce((a, b) => a + b) / arr.length;

module.exports = {
	// When being at 0 or MAX in Steps on an axis
	// Go to the next opposite endstop for determine
	// a new axis size
	measure_step_axis: Action((ITF) => (args) => ({
		step_measure: [],
		exec() {
			return [
				AO.move_until_endstop(args.axis, 'far')
			]
		},
		datas(data) {
			this.step_measure.push(data);
		},
		end() {
			if(this.step_measure.length === 0) return log.err('No step measure data');
			
			if(this.parent && this.parent.name === "calibrate") {
				this.notifyParent(this.step_measure); 
			} else {
				return new Promise(resolve => {
					DB.Axis
					.updateSizeInStep(args.axis, mean(this.step_measure.map(m => m.data)))
					.then(() => { resolve() })
				})
			}
		}
	})),

	calibrate: Action((ITF) => (args, that) => ({
		measures: {},
		exec() {
			// Resolution is the number of time we repeat
			// the measure process. 
			// When is > 1 the multiple measure values are meaned
			// before being stored in database.
			// When is 0 or undefined it's not doing any measure.
			if(!args.resolution) args.resolution = 1

			return args.axes.split('').map((axis) => {
				return [
					AO.move_until_endstop(axis, 'near'), // go to nearest endstop before doing actual measure
					Array(args.resolution).fill(AO.measure_step_axis(axis))
				]
			})
		},

		notify(child, data) {
			if(child.name === "measure_step_axis") {
				const axis = child.args.axis;

				if(!this.measures[axis]) this.measures[axis] = []
				this.measures[axis].push(data)
			}
		},

		saveInDB: () => new Promise((resolve, reject) => {
			const measures = that.measures;

			if(!measures || Object.keys(measures).length <= 0) {
				log.err('No measures, Abort saving new axis size in database');
				resolve();
				return;
			}

			const getMeanStepMeasure = axis => {
				return mean(measures[axis].map(group => {
					return mean(group.map(m => m.data))
				}))
			}

			const saveAllAxes = () => Object.keys(measures).map(axis => {
				return DB.Axis
					.updateSizeInStep(axis, getMeanStepMeasure(axis));
			})

			return Promise.all(saveAllAxes()).then(() => resolve())
		}),

		end() {
			return this.saveInDB();
		}
	})),
}

