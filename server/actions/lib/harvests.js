const Action = require('../action')
const AO = require('../actionObjects').actions

module.exports = {
	drill: Action(ITF => args => ({
		exec: () => [
			AO.start_rotating_motor('b'),
			AO.move_down_until_maxweight(args.maxWeight),
			AO.stop_rotating_motor('b')
		],
		pause() {
			
		},
		end() {
		}
	}))
}
