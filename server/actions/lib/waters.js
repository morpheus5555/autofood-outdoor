const Action = require('../action')
const AO = require('../actionObjects').actions

module.exports = {
	flow: Action(ITF => args => ({
		exec() {
			ITF.HW.pumps['p1']
				.flow(args.volume, args.delay)
				.then(ITF.end);
		},
		stop() {
			ITF.HW.pumps['p1'].off();
		}
	})),

	blow: Action(ITF => args => ({
		exec() {
			ITF.HW.pumps['p2']
				.flow(args.volume, args.delay)
				.then(ITF.end);
		},
		stop() {
			ITF.HW.pumps['p2'].off();
		}
	})),

	blow_on: Action(ITF => args => ({
		exec() {
			ITF.HW.pumps['p2']
				.flow(Number.MAX_SAFE_INTEGER, args.delay)
			
			ITF.end()
		},
	})),
	blow_off: Action(ITF => args => ({
		exec() {
			ITF.HW.pumps['p2'].off();
			ITF.end()
		}
	})),

	water: Action(ITF => args => ({
		exec() {
			const offset_tool = 20;
			return [
				AO.move_to_hex(args.x, args.y),
				AO.move_to('z', 0 + offset_tool),
				AO.flow(args.volume, args.delay),
				AO.move_to('z', 100),
			]
		}
	})),

	water_all: Action(ITF => args => ({
		exec() {
			return [
				function water_loop() {
					return {
						exec: () => {
						},
						end: () => new Promise(resolve => {
							// change food state in db
							resolve();
						})
					}
				}
			]
		}
	})),
}
