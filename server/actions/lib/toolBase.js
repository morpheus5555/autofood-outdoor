const Action = require('../action')
const AO = require('../actionObjects').actions


module.exports = {
	move_to_toolbase: Action(ITF => args => ({
		exec: () => DB.ToolBase
			.getPosition()
			.then(toolbasePos => [

				//AO.move_dir('y', 0),
				AO.move_to('y', 0),			
		//		AO.move_to('x', toolbasePos.x),
				AO.move_to('z', toolbasePos.z),
			])
	})),

	/**
	 * Move axis x to the right position
	 * to be above the tool `args.tool`,
	 * ready to hook/unhook it
	 */
	move_to_tool_on_toolbase: Action(ITF => args => ({
		exec: () => DB.ToolBase
			.getPositionOfTool(args.tool)
			.then(position =>
				AO.move_to('x', position)
			)
	})),


}
