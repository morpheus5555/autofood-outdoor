const Action = require('../action')
const AO = require('../actionObjects').actions

module.exports = {
	hook_tool: Action(ITF => args => ({
		exec: () => {
            return AO.move_to('a', 3000)
        }
    })),
	unhook_tool: Action(ITF => args => ({
		exec: () => {
            return AO.move_to('a', 0)
        }
    })),

    // get_tool: Action()

    // Cover the entire process of changing a tool
    change_tool: Action(ITF => args => ({
        exec: () => DB.Head
            .getCurrentTool()
            .then(currentTool => {
                // End it now if we already have the right tool
                if(currentTool && currentTool.name === args.tool) 
                    ITF.end();

                // If Head have currently no tool
                if(!currentTool) {
                    return [
                        AO.move_to('z', 5000),
                        AO.move_to_toolbase(),
                        AO.move_to_tool_on_toolbase(args.tool),
                        AO.move_to('a', 0),

                        AO.move_down_until_maxweight(300),
                        AO.hook_tool(),
                    ]
                }

                // If Head have currently a tool but no the right one
                else {
                    const actions = [
                        AO.move_to('z', 5000),
                        AO.move_to_toolbase(),
                        AO.move_to_tool_on_toolbase(currentTool.name),
                        AO.move_down_until_maxweight(300),
                        AO.unhook_tool(),
                        AO.move_to('z', 5000),
                    ]

                    if(args.tool !== "Nothing")
                        actions.concat([
                            AO.move_to('z', 5000),
                            AO.move_to_tool_on_toolbase(args.tool),
                            AO.unhook_tool(),
                            AO.move_down_until_maxweight(300),
                            AO.hook_tool(),
                            AO.move_to('z', 5000),
                        ])
                    
                    return actions
                }
            }),
        
            end: () => {
                DB.Head.selectTool(args.tool)
            }
        })),

    // (just a test) Read the value of the load cell one time then end
    get_loadcell_value: Action(ITF => args => ({
        exec: () => ITF.HW.loadCells[args.loadcell].read().then(value => {
            log.info(`Load cell ${args.loadcell} has a value of ${value.toGrams()}g (${value})`)
            ITF.end();
        })
    })),
    
    move_down_until_maxweight: Action(ITF => args => ({
        exec: () => {
            ITF.HW.loadCells['c1'].readContinuous(value => {
                return value.toGrams() > args.maxWeight
            }).then(value => {
                ITF.HW.axes['z'].stop();
                ITF.end();
            })

            return AO.move_dir('z', 0)
        },
        resume() {
            ITF.HW.loadCells['c1'].resumeReading();
        },
        pause() {
            ITF.HW.loadCells['c1'].pauseReading();
        },
        stop() {
            ITF.HW.loadCells['c1'].stopReading();
        }
    }))
}