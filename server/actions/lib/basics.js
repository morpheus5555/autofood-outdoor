const Action = require('../action')
const utils = require('../utils');
const getBot = require('../../getBot')
const BotModel = require('mongoose').model('Bot');
const AxisModel = require('../../models/axis').model  //require('mongoose').model('Axis');
const AO = require('../actionObjects').actions

function getFromObject(obj, keys) {
	let newObj = {};
	keys.map(key => {
		newObj[key] = obj[key];
	});
	return newObj;
}
const mean = (arr) => arr.reduce((a, b) => a + b) / arr.length;

const savePosition = (axis_name) => new Promise((resolve, reject) => {
	const HW = require('./../../hardware').components;
	const HWaxis = HW.axes[axis_name];
	const pos = HWaxis.getPos();

	AxisModel
		.findOne({ name: HWaxis.name })
		.then((axis, err) => {
			if(err) {
				reject(err);
			} else {
				const dir = HWaxis.getMeanMotorPos(true) > 0 ? 1 : 0;
				axis.position = pos;

				// set position to 0 if position < 0 OR
				// if axis did go in dir 0 and all its group's limit endstops are pushed,
				// useful when endstops which define limit 0 are pushed before position = 0
				if(dir === 0 && HWaxis.areAllGroupsLimitEndstopsPushed(dir) || axis.position < 0) {
					axis.position = 0;
					HWaxis.setPos(0);
				}
				
				axis.save((err) => {
					if(err) log.err(err);
					log.debug("Position saved: " + axis.position);
					require('../../socket').sendAxis(axis.name, axis)
					resolve(pos);
				});
				
			}
		})
});

module.exports = {

	move_motor_to: Action(ITF => args => {
		const motor = ITF.HW.motors[args.motor]
		return {
			exec() {
				motor.moveTo(args.pos, ITF.end)
				M.MOVING(motor.name,args.pos)
			},
			stop: () => motor.stop(),
			resume() {
				motor.resume();
			},
			pause: () => motor.pause(),
			end: () => {
				DB.Motor.savePosition(args.motor, args.pos)
			}	
			
		//	 end: () => 	that.notifyParent(meanPos)
			//	.then(() => savePosition(args.axis)),
		/*	end: () => savePosition(args.axis)
				.then(meanPos => {
					that.notifyParent(meanPos);
				})
				.catch(err => {
					log.err(err);
				})*/
		}
	}),

	// Absolute move function
	move_to: Action((ITF) => (args, that) => ({
		exec() {
			ITF.HW.axes[args.axis].moveTo(args);
			M.MOVING(args.axis,args.pos)
		},
		stop: () => ITF.HW.axes[args.axis].stop()
			.then(() => savePosition(args.axis)),
		resume() {
			ITF.HW.axes[args.axis].resume();
		},
		pause: () => ITF.HW.axes[args.axis].pause()
			.then(() => savePosition(args.axis)),
		end: () => savePosition(args.axis)
			.then(meanPos => {
				that.notifyParent(meanPos);
			})
			.catch(err => {
				log.err(err);
			})
	})),

	// Relative move function
	move: Action((ITF) => (args) => ({
		exec() {
			const rArgs = Object.assign({}, args, {
				pos: ITF.HW.axes[args.axis].getPos() + args.pos
			});

			return AO.move_to(rArgs.axis, rArgs.pos, rArgs.speed, rArgs.acc);
		},
	})),

	// Move without listening to sensors (IR)
	// It is mainly used for tests
	simple_move: Action((ITF) => (args) => ({
		exec() {
			//ITF.Serial.sendAction(`simple_move_${args.axis}`, args);
		}
	})),

	move_to_home: Action((ITF) => (args) => ({
		exec() {
			return AO.move_dir(args.axis, 0)
		}
	})),
	move_to_home_all: Action((ITF) => (args) => ({
		exec() {
			return [
				AO.move_dir('a', 0),
				AO.move_dir('x', 0),
				AO.move_dir('y', 0),
				AO.move_dir('z', 0)
			]
		}
	})),
	move_to_max: Action((ITF) => (args) => ({
		exec() {
			return AO.move_dir(args.axis, 1, args.speed, args.acc)
		}
	})),
	move_to_max_all: Action((ITF) => (args) => ({
		exec() {
			return [
				AO.move_dir('a', 1),
				AO.move_dir('x', 1),
				AO.move_dir('y', 1),
				AO.move_dir('z', 1)
			]
		}
	})),

	move_to_hex: Action((ITF) => (args) => {
		const hexPos = { x: args.x, y: args.y }

		return {
			exec: () => utils.hexagonToStep(args.x, args.y).then((steps) => [
				AO.move_to('y', steps.y/*, args.speed, args.acc*/),
				AO.move_to('x', steps.x/*, args.speed, args.acc*/)
			]),
			end: () => new Promise(resolve => {
				getBot(bot => {
					bot.setHexPosition(hexPos, () => {
					
						resolve();
					})
				})
			})
		}
	}),

	next_food: Action((ITF) => (args) => new Promise((resolve) => {
		const run = (action) => getBot((bot) => {
			bot.getNextFood(1, (food) => {
				resolve(action(food));
			});
		});

		run(food => ({
			exec() {
				if(!food || food === 'no_pos') {
					// log.info('crappy crap');
					return { name: "move_to_hex", args: {x:0,y:0}}
				}

				return {
					name: "move_to_hex",
					args: { x: food.position.x, y: food.position.y }
				}
			},
			end() {
			}
		}))
	})),

	move_until_endstop: Action((ITF) => (args) => ({
		exec: () =>
			AxisModel
				.getDirection(args.axis, args.target)
				.then(direction =>
						AO.move(args.axis, direction === 0 ?
							Number.MIN_SAFE_INTEGER : Number.MAX_SAFE_INTEGER)
				)
	})),

	move_dir: Action((ITF) => (args) => ({
		exec: () => {
			let pos = Number.MAX_SAFE_INTEGER
			if(args.dir === 0)
				pos = Number.MIN_SAFE_INTEGER
			return AO.move(args.axis,  pos)
		}
	})),

	// Rotate motors that are not in an axis
	// Litlle dangerous because it can only be stopped with stop_rotating_motor
	// start_rotating_motor: Action(ITF => args => ({
	// 	exec: () => {
	// 		ITF.HW.motors[args.motor].move(args.dir === 1 ?
	// 			Number.MAX_SAFE_INTEGER : Number.MIN_SAFE_INTEGER)
	// 		ITF.end();
	// 	}
	// })),

	// stop_rotating_motor: Action(ITF => args => ({
	// 	exec: () => {
	// 		ITF.HW.motors[args.motor].stop();
	// 		ITF.end();
	// 	}
	// }))
}
