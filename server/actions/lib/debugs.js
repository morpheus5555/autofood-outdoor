const Action = require('../action')
const Axis = require('./../../models/axis').model;
const AO = require('../actionObjects').actions



module.exports = {
	dumb_count: Action(ITF => (args) => {
		const n_group = ITF.HW.axes[args.axis].groups.length;
		const n_motor = ITF.HW.axes[args.axis].groups[0].motors.length;


		const meanDatas = (datas) => {
			let meaned = {};
			Object.keys(datas).forEach(groupName => {
				const meanMotorsValue = _.mean(datas[groupName])

				meaned[groupName] = Math.round(Math.abs(meanMotorsValue));
			})
			return meaned;
	  	}

		const cleanLastDatas = (datas) => {
			const err = (e) => {
				log.err('Abort dumb count -> ' + e);
				return false;
			}

			if(Object.keys(datas).length < n_group)
			  return err('Missing group(s) in datas');

			if(Object.keys(datas).length > n_group)
			  return err('Too much group(s) in datas');
			
			for(group in datas) {
				if(datas[group].length < n_motor)
					return err('Missing motor(s) in datas');

				if(datas[group].length > n_motor)
					return err('Too much motor(s) in datas');

				if(datas[group].indexOf(0) !== -1)
					return err('A data is equal to 0')
			}
			
			return true;
		}

		const save = (datas) => new Promise(resolve => {
			const meanedDatas = meanDatas(datas);

			return DB.Group.find({
				'name': {
				  '$in': Object.keys(meanedDatas)
				}
			})
			.then(groups => Promise.all(groups.map(group => {
				group.datas.step_measure.push(meanedDatas[group.name]);
				return group.save();
			})).then(() => resolve()))
		})


		return {
			exec: () => [
				AO.move_until_endstop(args.axis, 'near', args.speed, args.acc),

				function loop_to_end() {
					let datas = {};

					return {
						exec: () => [
							AO.measure_step_axis(args.axis, 'far', args.speed, args.acc)
						],
						datas(data) {
							const groupName = data.axisName + data.groupIndex

							if(!datas[groupName]) datas[groupName] = [];
							
							datas[groupName].push(data.data);
						},
						end() {
							if(!cleanLastDatas(datas)) return;

							return save(datas).then(() => {
								args.iterations--;

								if (args.iterations <= 0) return;

								return loop_to_end;
							});
						}
					}
				}
			]
		}
	})
}
