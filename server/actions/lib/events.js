const Group = require('./../../models/group').model;
const H = require('../../hardware').components;


module.exports =  {
    emulate: {
        push_endstop: (args) => {
            if(args.index > H.axes[args.axis].groups.length - 1) return;

            H.axes[args.axis].groups[args.index].endstops.forEach((endstop,i) => {
                if(args.pushed[i] === 1) endstop.fakePush()
            })

            H.endstops.list.forEach(e => e.unfake());
        },
        stop: () => {
            require('./../actionManager').receive('stop');
        },
        pause: () => {
            require('./../actionManager').receive('pause');
        },
        resume: () => {
            require('./../actionManager').receive('resume');
        },
    },

    end: () => {
        require('./../actionManager').next();
    },
    stop: () => {
        require('./../actionManager').stop();
    },
    // resume: () => {
    //     require('./../actionManager').resume();
    // },
    // pause: () => {
    //     require('./../actionManager').pause();
    // },
    datas: (args) => {
        // log.info(args);
        //Group.findOne({ name: args.axis.toLowerCase() + args.index }, function(err, group) {
            //group.datas.step_measure.push(args.tiny_step_measure);
            //group.save();
        //});
    },


    push_endstop:  (args) => {
        const group_name =  args.axis.toLowerCase() + args.index;
        
        Group.findOneAndUpdate({ name: group_name },
                                                { upsert: true })
                                                .then((group) => {

            if(group === null) {
                log.err(`Group ${group_name} not found`);
                return;
            }
            group.endstops = args.pushed = args.pushed.map((e,curr) =>  Boolean(e)); // [int] to {bool}

            group.save((err) => { // remind: better error handling ..
                if (err) return log.err(err);
            });

            args.type = "partial_mutation";
            args.endstops = group.endstops;
            args.element = "endstops"; // remind: can be removed if we use the key of object
            args.axis  = args.axis.toLowerCase();

            // Can surely be cleaner
            require('../../socket').broadcast(JSON.stringify(args));
        
        }).catch((err) => {
            log.info(err);
        });
    },
    
}
