const Action = require('../action')
const AO = require('../actionObjects').actions

module.exports = {
	// Restart the node process
	"restart":  Action((ITF) => (args) => ({
		exec() {
			process.exit();
		}
	})),

	"repo_update": Action((ITF) => (args) => ({
		exec() {
			//exec_cmd('git fetch --all');
		}
	})),

	// Reboot the RaspberryPI
	"reboot":  Action((ITF) => (args) => ({
		exec() {
			exec_cmd('sudo /sbin/shutdown -r now');
		}
	}))
}
