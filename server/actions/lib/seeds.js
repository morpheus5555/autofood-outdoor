const Action = require('../action')
const AO = require('../actionObjects').actions
const utils = require('../utils');


// const patterns = () => {
// 	return {
// 		square: (...foodtype) => {
// 			return [
// 				[]
// 			]
// 		},
// 	}
// }

// const transclude = (pattern_name) => {
// 	this[pattern_name]
// }



function seed_loop(slots, foodTypes) {
  return function seed_em_all() {
    const pos = slots[0].position;

    return {
      exec: () => [
        AO.seed(pos.x, pos.y, 'radish')
      ],
      end() {
        slots.shift();

        if (slots.length > 0) {
          return seed_em_all;
        } else {
          log.info('Seeded everything');
        }
      }
    }
  }
}

//const  isBlowerRetracted = () =>	ITF.HW.motors.d.getPosition() !== 0

module.exports = {
  seed: Action((ITF) => (args) => {
    let SlotHasFood = false;
    return {
      before() {

      },
      exec: () =>
        Promise.all([
          DB.Slot.hasFood({
            x: args.x,
            y: args.y
          }),
          DB.FoodType.findOne({
            name: args.foodtype
          })
        ]).then(([hasFood, foodtype]) => {
          SlotHasFood = hasFood

          if(hasFood) {
            log.err('Cannot seed here, this slot already has food')
            return ITF.end();
          }
  
          const offset_tool = 20;
          return [
            AO.move_to_hex(args.x, args.y),
            AO.move_to('z', 0 + offset_tool), // go down

			AO.select_seed_type(args.foodtype),
			
			// Take the seed
            AO.detract_blower(),
            AO.blow_on(0),
            AO.retract_blower(),
			AO.blow_off(0),
			
			// Put it in soil
            AO.flow(30, 400),

			// Go up
            AO.move_to('z', P.SAFE_Z_POS), 
          ]
        }),
      end: () => new Promise(resolve => {
        if (SlotHasFood) return resolve();

        DB.Slot.seed({
          type: args.foodType,
          position: {
            x: args.x,
            y: args.y
          }
        }).then((food) => {
          DB.FoodType.inc_instances(food.type)
          Socket.seedFood(food, {
            x: args.x,
            y: args.y
          })
          log.success(`Seeded ${args.foodType} on [${args.x},${args.y}]`);
          resolve();
        })
      })
    }
  }),


  select_seed_type: Action(ITF => (args) => {
    return {
      exec: () =>
        Promise.all([
          // Check if we have the right seed type in the chunk	
          DB.Seeder.checkSeeds(args.seedName),

          // Get the steps needed to move the A Axis
          // to the right angle for rotating compartment
          DB.Seeder.getStepForAngle(args.seedName),

        ]).then(([hasSeed, steps]) => {
          const actions = []

          if (!hasSeed)
             ITF.end()
          else if (steps == NaN)
            ITF.end()

          return [
            AO.change_tool('Seeder'),
            AO.retract_blower(),
            AO.move_motor_to('b', 0),
            AO.move_motor_to('b', steps),
          ]
        }),
    }
  }),

  retract_blower: Action(ITF => args => ({
    exec: () =>
      DB.Head.hasTool("Seeder")
      .then(hasTool => {
        if (hasTool)
          return AO.move_motor_to('d', 0)
        else {
          M.BLOWER_WRONG_TOOL()
          ITF.end()
        }
      })

  })),

  detract_blower: Action(ITF => args => ({
    exec: () =>
      DB.Head.hasTool("Seeder")
      .then(hasTool => {
        if (hasTool)
          return AO.move_motor_to('d', args.pos)
        else {
          M.BLOWER_WRONG_TOOL()
          ITF.end()
        }
      })

  })),

  seed_all: Action(ITF => (args) => {
    return {
      exec: () => Promise.all([
        DB.Slot.getEmpties(),
        DB.FoodType.getIds()
      ]).then(values => [
        //AO.calibrate(),
        seed_loop(values[0], values[1])
      ])
    }
  })
}
