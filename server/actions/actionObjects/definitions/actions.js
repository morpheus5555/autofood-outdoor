const actionsDefinition = () => ({
	basics: {
		move: ['axis', 'pos', 'speed', 'acc'],
		simple_move: ['axis', 'pos', 'speed', 'acc'],
		move_motor_to: ['motor', 'pos'],
		move_to: ['axis', 'pos', 'speed', 'acc'],
		move_to_home: ['axis', 'pos', 'speed', 'acc'],
		move_to_home_all: [],
		move_to_max: ['axis', 'speed', 'acc'],
		move_to_max_all: [],
		move_to_hex: ['x', 'y', 'speed', 'acc'],
		move_until_endstop: ['axis', 'target', {'speed':100}, 'acc'],
		move_dir: ['axis', 'dir', 'speed', 'acc'],
		next_food: [],
    },
	calibrations: {
		measure_step_axis: ['axis', 'speed', 'acc'],
		calibrate: [{'axes':'xyz'}, {'resolution':1}],
		calibrate_head: [],
	},
	head: {
		change_tool: ['tool'],
		hook_tool: [],
		unhook_tool: [],
		get_loadcell_value: ['loadcell'],
		move_down_until_maxweight: ['maxWeight'],
	},
	toolbase: {
		move_to_toolbase: [],
		move_to_tool_on_toolbase: ['tool'],
	},
	seeds: {
		seed: ['x', 'y', 'foodType', 'speed', 'acc'],
		seed_all: [],
		select_seed_type: ['seedName'],
		seed_in_soil: [],
		retract_blower: ['speed'],
		detract_blower: ['pos', 'speed']
	},
	waters: {
		flow: ['volume', 'delay'],
		blow: ['volume', 'delay'],
		blow_on: ['delay'],
		blow_off: ['delay'],
		water: ['x', 'y', 'volume', 'delay', 'speed', 'acc'],
		water_all: [],
	},
	harvests: {
		harvest_one: ['x', 'y', 'speed', 'acc'],
		harvest_all: [],
	},
	debugs: {
		dumb_count: ['axis', {'iterations':2}, 'dir', 'speed'],
	},
	maintenances: {
		restart: [],
		repo_update: [],
		reboot: [],
	}
})

actionsDefinition.withoutCategories = () => {
	const def = actionsDefinition();

	return R.reduce((acc, categoryName) => {
		return R.merge(acc, def[categoryName]);
	}, {}, R.keys(def));
}

module.exports = actionsDefinition;