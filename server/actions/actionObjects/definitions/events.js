const eventsDefinition = () => ({
  // state: category({
  //     end: [],
  //     datas: [],
  // }),
  queue: {
    stop: [],
    pause: [],
    resume: []
  },
  hardware: {
    push_endstop: ['axis', { pushed: [0, 1, 0] }, 'index']
  }
})

eventsDefinition.withoutCategories = () => {
	const def = eventsDefinition();

	return R.reduce((acc, categoryName) => {
		return R.merge(acc, def[categoryName]);
	}, {}, R.keys(def));
}

module.exports = eventsDefinition;