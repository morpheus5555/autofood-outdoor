module.exports = ({
	axesName,
	foodTypesName,
	toolsName,
	seedsName,
	loadcells,
	motorsName
} = props) => ({
	axis: {
		default: axesName[0],
		type: 'string',
		select: axesName
	},
	motor: {
		default: 'd',
		type: 'string',
		select: ['a','b','c','d','x1','x2','y1','y2','z1','z2'], 
	},
	pos: {
		default: 200,
		type: 'number',
		unit: 'step'
	},
	speed: {
		default: 150,
		type: 'number',
		min: 0,
		unit: 'step/sec',
		modifiers: R.map(axisName => ({
			arg: 'axis',
			value: axisName,
			default: P[axisName.toUpperCase() + '_SPEED']
		}), axesName)
	},
	acc: {
		default: 250,
		type: 'number',
		min: 0,
		max: 300,
		unit: 'step/sec²',
		modifiers: R.map(axisName => ({
			arg: 'axis',
			value: axisName,
			default: P[axisName.toUpperCase() + '_ACC']
		}), axesName)
	},
	x: {
		default: 0,
		type: 'number',
		min: 0,
		unit: 'hex'
	},
	y: {
		default: 0,
		type: 'number',
		min: 0,
		unit: 'hex'
	},
	dir: {
		default: 0,
		type: 'number',
		select: [0,1]
	},
	save: {
		default: true,
		type: 'boolean'
	},
	volume: {
		default: 30,
		type: 'number',
		min: 0,
		unit: 'milliliter'
	},
	delay: {
		default: 500,
		type: 'number',
		min: 0,
		unit: 'ms'
	},
	target: {
		default: 'near',
		type: 'string',
		select: ['near', 'far']
	},
	index: {
		default: 0,
		type: 'number',
		min: 0,
		max: 4
	},
	tool: {
		default: toolsName[0],
		type: 'string',
		select: toolsName
	},
	foodType: {
		default: foodTypesName[0],
		type: 'string',
		select: foodTypesName
	},
	seedName: {
		default: seedsName[0],
		type: 'string',
		select: seedsName
	},
	loadcell: {
		default: loadcells[0],
		type: 'string',
		select: loadcells
	},
	maxWeight: {
		default: 500,
		type: 'number',
		unit: 'grams',
		min: 0,
		max: 10000
	},
	motor: {
		default: motorsName[0],
		type: 'string',
		select: motorsName
	}
})
