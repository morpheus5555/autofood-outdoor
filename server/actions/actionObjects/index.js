const argumentsDef = require('./definitions/arguments');
const actionsDef = require('./definitions/actions');
const eventsDef = require('./definitions/events');

const ArgChecker = require('./ArgChecker')

const definition = {};
const actions = {};
const events = {};

let inited = false;

/**
 * @return {Object} Object passed to definitions/arguments.js
 */
const getArgumentsProps = () => {
	const HW = require('../../hardware').components;
	return Promise.all([
		DB.Axis.find({}).select('name').lean(),
		DB.Tool.find({}).select('name').lean(),
		DB.FoodType.find({}).select('name').lean(),
		DB.Seeder.findOne({}).deepPopulate('chunks.type').lean()
	]).then(([axes, tools, foodTypes, seeder]) => ({
		axesName: axes.map(axis => axis.name),
		toolsName: tools.reduce((acc,tool) => acc.concat([tool.name]), ['Nothing']),
		foodTypesName: foodTypes.map(foodType => foodType.name),
		seedsName: seeder.chunks.reduce((acc, seedChunk) => {
			if(seedChunk.type) acc.push(seedChunk.type.name)
			return acc;
		}, []),
		loadcells: HW.loadCells.list.map(loadcell => {
			return loadcell.name;
		}),
		motorsName: HW.motors.list.reduce((acc, motor) => {
			if(motor.groupIndex === null) acc.push(motor.name);
			return acc;
		}, [])
	}))
}

/**
 * Create actions and events using elements from database, like axes name,
 * to feed arguments. Can be called multiple times to update definitions.
 */
function init() {
	return getArgumentsProps().then(argumentsProps => {
		Object.assign(definition, { // arguments is before because actions and events need it
			arguments: argumentsDef(argumentsProps)
		})

		Object.assign(definition, {
			actions: categories(actionsDef()),
			actionsOnly: actionsDef.withoutCategories(),
			events: categories(eventsDef()),
			eventsOnly: eventsDef.withoutCategories(),
		})

		Object.assign(actions, getActionsObjects('actions'))
		Object.assign(events, getActionsObjects('events')) 
		inited = true;
	})
}

/**
 * Convert argument name or 'inline definition' (e.g. { pushed: [1,0,1] })  to
 * argument definition (e.g. { name: 'pushed', type: 'array', default: [1,0,1] })
 * as in definitions/arguments.js
 * @param {string|Object} argNameOrDefinition 
 */
function argumentToDefinition(argNameOrDefinition) {
	if(is.string(argNameOrDefinition)) // is the argument name
		return Object.assign({}, definition.arguments[argNameOrDefinition], {
			name: argNameOrDefinition
		})
	
	if(is.object(argNameOrDefinition)) { // is an 'inline' argument definition, like { pushed: [0,1,0] }
		const name = Object.keys(argNameOrDefinition)[0]
		return Object.assign({}, definition.arguments[name], {
			name,
			default: argNameOrDefinition[name],
			type: is.array(argNameOrDefinition[name]) ? 'array' : typeof argNameOrDefinition[name]
		})
	}
}

/**
 * Change each actions in categories from definitions/actions.js or definitions/events.js
 * into actions definitions where arguments aren't an array of name,
 * but an array of argument definition (object with attributes like default, min, max, ...),
 * which are in definitions/arguments.js
 * @param {Object} categoryObjects 
 * @return {Object}
 */
function categories(categoryObjects) {
	return Object.keys(categoryObjects).reduce((categoriesAcc, categoryName) => {
		const categoryDefinition = categoryObjects[categoryName];

		const category = Object.keys(categoryDefinition).reduce((acc, action) =>
			Object.assign(acc, { [action]: categoryDefinition[action].map(argument =>
					argumentToDefinition(argument) // for each argument of each action
				)
			})
		, {})

		return Object.assign(categoriesAcc, { [categoryName]: category })
	}, {})
}


/**
 * Apply modifiers to an argument definition. A modifier is an object which can change an
 * argument definition according to another argument value, they are definied
 * inside arguments, see definitions/arguments.js
 * @param {Object} argumentsValue e.g. { axis: 'x', speed: 200 }
 * @param {Array} argumentDefinition 
 */
function applyModifiers(argumentsValue, argumentDefinition) {
	if(is.undefined(argumentDefinition.modifiers)) return argumentDefinition;

	const getModifiedValues = R.reduce((acc, modifier) =>
		modifier.value !== argumentsValue[modifier.arg] ? acc :
			R.merge(acc, R.omit(['arg', 'value'], modifier))
	, {})

	return R.merge(
		argumentDefinition,
		getModifiedValues(argumentDefinition.modifiers)
	)
}
/**
 * Use action name and arguments value to construct an args object like:
 * [500,5] => { speed: 500, y: 5 },
 * may not be the final one used by the action
 * @param {String} name action/event name, e.g. seed
 * @param {String} type 'action' or 'event'
 * @param {Array} argsValue e.g. [0, 0, 'radish']
 */
function constructArgsObject(name, type, argsValue) {
	const def = type === 'action' ? definition.actionsOnly[name] : definition.eventsOnly[name];
	return R.zipObj(def, argsValue);
}

/**
 * Return the function used when we do a AO.action(args), also used for events
 * @param {string} name 
 * @param {string} type 'action' or 'event'
 * @param {Object} argumentsDefinition definitions of all arguments of the action
 */
function actionObject(name, type, argumentsDefinition) {
	return function(...args) {
		args = is.object(args[0]) ? args[0] : constructArgsObject(name, type, args);

		const applyAllModifiers = R.map(R.curry(applyModifiers)(args));

		const _arguments = ArgChecker.checkAll(args, applyAllModifiers(argumentsDefinition));

		// return Error instance (via log.err) so that the current action is stopped
		if(is.error(_arguments))
			return log.err(`Error on ${type} ${name}: ${_arguments.message}`);
		
		return { name, args: _arguments }
	}
}

/**
 * Return an object containing all actionObjects (also used for events),
 * which is the AO in AO.action(args)
 * @param {string} type 'actions' or 'events'
 */
const getActionsObjects = (type) => Object.keys(definition[type]).reduce((acc, category) => {
	Object.keys(definition[type][category]).map(action => 
		Object.assign(acc, {
			[action]: actionObject(action,
				type === 'actions' ? 'action' : 'event',
				definition[type][category][action])
		})
	)

	return acc;
}, {});

module.exports = {
	init,
	inited,
	definition,
	actions,
	events
}
