/**
 * Check if the argument value is corresponding to its definition (type, min/max, ...),
 * return the value to use in the action if valid,
 * return Error instance otherwise
 * @param {Object} argument
 * @param {*} value
 */
function check(argument, value) {
	// if value is undefined, return default from definition (if exist)
	if(is.undefined(value)) {
		return is.not.undefined(argument.default) ? argument.default : Error(
			`Value of argument ${argument.name} is undefined and the definition doesn't have a default`
		)
	}

	// is value of the expected type ?
	if(is.not[argument.type](value)) return Error(
		`Argument ${argument.name} only accept values of type ${argument.type}, ` +
		`but got ${value} (of type ${typeof value}) instead`
	);

	// is value one of the acceptable possibility ? (inside select array)
	if(is.not.undefined(argument.select)) {
		const isInSelect = argument.select.findIndex(el => el === value) !== -1;
		if(!isInSelect) return Error(
			`Value \`${value}\` is not an acceptable for argument ${argument.name}, ` +
			`only these are: ${JSON.stringify(argument.select)}`
		);
	}

	if(argument.type === 'number') {
		// is value < minimum
		if(argument.min !== undefined && value < argument.min) return Error(
			`Argument ${argument.name} has a minimum of ${argument.min}, but value is ${value}`
		)
		// is value > maximum
		if(argument.max !== undefined && value > argument.max) return Error(
			`Argument ${argument.name} has a maximum of ${argument.max}, but value is ${value}`
		)
	}

	return value;
}

/**
 * Check that ALL arguments value are corresponding to their definitions,
 * basically, it's a loop over #checkArgument
 * @param {Array|Object} arguments
 * @param {Object} argumentsDefinition definitions of all arguments of the action
 * @return {Error|Object}
 */
function checkAll(arguments, argumentsDefinition) {
	assert(is.array(arguments) || is.object(arguments),
		`arguments can only be an array or an object, but is actually of type: ${typeof arguments}`
	)
	
	// Check all arguments and make it an object
	const checkedArguments = argumentsDefinition.reduce((acc, argumentDef, i) =>
		Object.assign(acc, {
			[argumentDef.name]: check(argumentDef, (
				is.array(arguments) ? arguments[i] : arguments[argumentDef.name]
			))
		})
	, {});

	// If one of arguments value is an Error instance
	const error = Object.values(checkedArguments).find(is.error)

	return error || checkedArguments;
}

module.exports = { checkAll }