;

module.exports = function ActionNode(params) {
	const instance_params = {
		id: params.id || Math.floor(Math.random() * Math.floor(Number.MAX_SAFE_INTEGER)) , // not cryptographically secure  yet, but really high number range so it's okay
		name: params.name,
		args: params.args,
		parent: params.parent,
		childrens: params.childrens || [],
		actionObject: params.actionObject,
		notifyParent: (data) => {
			if(this.parent !== undefined && this.parent !== null)
				this.parent.notify(this, data)
			else log.info("Cannot notify: Action has no parent", 0);
		},
		notify() {},

		// Notify the parent by default
		datas: (data) => {
			this.notifyParent(data)
		},
		before: () => {},
		stop: () => this.end_action(),
		end() {},
		end_action() {
			return this.end()
		}
	}

	const add = (...objs) => Object.assign(this, ...objs);

	const instantiate = () => new Promise((resolve) => {
		const instance = params.actionObject(params.args, this);

		if(instance instanceof Promise) {
			return instance.then((action) => {
				resolve(add(action));
			});
		} else {
			resolve(add(instance));
		}

	});

	return add(instance_params, { instantiate })
}
