const exec_cmd = require('child_process').exec;
const actionslist = require('./actions');
const events = require('./lib/events');
const ActionNode = require('./actionNode');
const AO = require('./actionObjects')

function flatten(arr) {
	return arr.reduce(function (flat, toFlatten) {
		return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
	}, []);
}

function ActionQueue(parentQueue = null) {
	let queue = []; // contain ActionNode instances
	let current = null;

	const broadcast = require('../socket').broadcast

	// Execute the current action in queue
	function step() {
		this.current = current = queue[0];
		
		if(current === undefined) {
			log.err('current action in queue is undefined');
			return;
		}

		if(!current.exec) {
			return current.instantiate().then(step);
		}

		if(!current.subQueue) {
			const request = current.exec();
			if(request) createSubQueue(request);
		} else {
			current.subQueue.step();
		}
	}

	const createSubQueue = (request) => {
		this.current = current;

		if(request instanceof Error)
			return receive('stop');

		current.subQueue = new ActionQueue(this);
		current.subQueue.insert(request).then(() => {
			if(current && !current.subQueue.isEmpty())
				current.subQueue.step();
		});
	}

	// Transform action request to ActionNode instances
	// { name, args } or [{ name, args }] -> [ActionNode]
	function requestToActionNode(request) {
		// request -> [request]
		if(!Array.isArray(request)) {
			request = [request];
		}

		request = flatten(request);

		const parser = item => {
			if(typeof item === 'object') {
				return createActionNode(item);
			} else if(typeof item === 'function') { // inline action
				return createActionNode(item, { actionObject: item })
			}
		}

		return request.map(parser);
	}

	function createActionNode(item, options = {}) {
		const node = new ActionNode(Object.assign({}, {
			actionObject: actionslist.get(item.name),
			parent: parentQueue ? parentQueue.current : null,
			name: item.name
		}, item, options));

		if(parentQueue && parentQueue.current !== null)
			// Update the parent node childrens array
			// If it has one
			parentQueue.current.childrens.push(node);
		
		return node;
	}

	function deleteCurrent () {
		this.current = current = null;
		queue.shift();

		// delete in parentQueue -> childrens
		if(parentQueue && parentQueue.current !== null)
			parentQueue.current.childrens.shift();
	}

	function isSubQueueEmpty() {
		return current === null || current.subQueue === undefined || current.subQueue.isEmpty();
	}

	// Remove current action and execute the following
	function next() {
		if(current === null) { // Useful if we receive an emulated event wihtout a current action.
			log.info('No Current Action. Abort "next".')
			return;
		}

		if(isSubQueueEmpty()) {
			const current_id = current.id;
			deleteCurrent();
			if(queue.length > 0)
				step();
			else {
				if(parentQueue) {
					parentQueue.receive('end');
				}
			}
			require('../socket').deleteQueueItem(current_id);
		}
		else {
			current.subQueue.next();
		}
	}

	// insert in queue as last
	function append(request) {
		requestToActionNode(request).map(item => queue.push(item));
	}
	
	// insert at queue index 1 (just after the current action)
	function prepend(request) {
		requestToActionNode(request).reverse().map(item => queue.splice(1, 0, item));
	}

	// Promise resolved when action request is appended/prepended into the queue
	function insert(request, prepending = false) {
		return new Promise((resolve,reject) => {
			const push = (_request) => {
				if(!_request) return resolve();

				if(!prepending) append(_request);
				else prepend(_request);

				require('../socket').sendQueue();

				resolve();
			}

			if(request instanceof Promise) {
				request.then(push);
			} else {
				push(request);
			}
		})	.catch(err => {
			log.err(err);
		});
	}

	// Execute current's event, recursively to the first parent, then execute events[name]
	function receiveRecurse(name, args) { 
		if(!current) {
			return events[name] ? events[name](args) : false;
		}

		const propagateEvent = () => {
			if(!parentQueue) {
				if(events[name]) events[name](args);
			} else {
				parentQueue.receiveRecurse(name, args);
			}
		}

		if(current[name]) {
			// end is only applied to the deepest current action
			if(name === 'end') {
				if(isSubQueueEmpty()) {
					const act = current.end_action();
					if(act) {
						return insert(act, true)
							.then(propagateEvent);
					}
				}
			} else {
				const act = current[name](args);
				if(act instanceof Promise) {
					return act.then(propagateEvent)
				}
			}
		}
		propagateEvent();
	}

	// Execute deepest queue's function `fnName` with `args` arguments
	const fromDeepestQueue = (fnName, ...args) => {
		return isSubQueueEmpty() ? this[fnName](...args) : current.subQueue.fromDeepestQueue(fnName, ...args);
	}

	function deleteAction(id) {
		queue.map((item, index) => {			
			if(item.subQueue) // Check recursively in each child queue
				item.subQueue.deleteAction(id);

			if(item.id === id) {
				let deleted_item = queue.splice(index, 1)[0];
				require('../socket').deleteQueueItem(deleted_item.id);
			}
		})

	}


	function receive(name, args) {
		return fromDeepestQueue('receiveRecurse', name, args)
	}

	return Object.assign(this, {
		step,
		receive,
		receiveRecurse,
		insert,
		next,
		current,
		size: () => queue.length,
		queue,
		deleteCurrent,
		isSubQueueEmpty,
		requestToActionNode,
		fromDeepestQueue,
		deleteAction,
		isEmpty: () => queue.length === 0
	})
}

function QueueFormatter(queue) {

	// Constitute the Hierarchy of wanted values, recursively 
	function formattedQueue(nodes) {
		//log.info(nodes);
		return nodes.map((el,i,arr) => {
			return {
				id: el.id,
				name: el.name,
				args: el.args,
				//parent: el.parent,
				childrens: formattedQueue(el.childrens),
			}
		})
	}

	// Get the formatting version of the Queue list of the bot
	// For sending it to the client and preventing circular structure and etc
	function JSONFormat() {
		return JSON.stringify(formattedQueue(queue.queue));
	}

	return {
		Queue: () => formattedQueue(queue.queue),
		JSON: JSONFormat,
	}
}

function ActionManager() {
	const queue = new ActionQueue();

	function incomingAction(request) {
		if(actionslist.exist(request.name)) {
			// execute actionObject to check arguments
			const requestAO = AO.actions[request.name](request.args);
			if(is.error(requestAO)) return;

			queue.insert(requestAO).then(() => {
				if(queue.size() === 1)
					queue.step();
			});
		}
		// Or directly launch (emulation !) event
		else if(Object.keys(events.emulate).includes(request.name)) {
			const requestAO = AO.events[request.name](request.args);
			if(is.error(requestAO)) return;

			events.emulate[request.name](request.args); 
		}
		else {
			log.info(`Action or event ${request.name} don't exist`);
		}
	}

	function receive(name,args) {
		queue.receive(name,args);
	}

	function next () {
		printHierarchy();
		queue.next();
	}

	function stop() {
		if(queue.size() <= 0) return;

		require('../socket').deleteQueueItem(queue.current.id);
		queue.deleteCurrent();
		if(queue.size() > 0) queue.step();
	}

	// Make an array of all parents of current action until itself
	function getHierarchy(q = queue) {
		const current = q.queue[0];

		if(current === undefined) return [];

		return q.isSubQueueEmpty()
			? [current.name]
			: [current.name, ...getHierarchy(current.subQueue)]
	}

	function printHierarchy() {
		const message = getHierarchy().join(' -> ');
		if(message.length !== 0) 
		log.info(message, 0);
		/*else  if( && VERBOSE_LEVEL >= 2)
			log.info("Hierarchy is Empty"); */      // For Future (todo)
		
	}

	function deleteAction(id) {
		queue.deleteAction(id)
	}

	return {
		deleteAction,
		incomingAction,
		receive,
		queue,
		stop,
		next,
		QueueFormatter: QueueFormatter(queue),
	}
}

module.exports = ActionManager()