/*
 * Module which handle all modifiable parameters,
 * and their update in database
 */

const paramsList = require('./list')

/**
 * Object containing each parameters value, ordered by name.
 * Used when you use the global variable P
 */
let params = Object.assign({}, paramsList);

/**
 * Create parameters in database. If there is a new parameter,
 * it will add it without modifying others, so that you can just modify list.js
 * to add a parameter
 */
function createInDB() {
	return DB.Parameter.find({})
		.then(dbParameters => {
			const getParamsInDB = () => R.map(R.prop('name'))(dbParameters)
			const getMissingParams = R.difference(R.keys(paramsList))
			const pickParamsValue = R.flip(R.pick)(paramsList);
			const formatParams = R.compose(
				R.values,
				R.mapObjIndexed((paramValue, paramName) => ({
					name: paramName,
					value: paramValue
				}))
			)

			const getParamsToInsert = R.compose(
				formatParams,
				pickParamsValue,
				getMissingParams,
				getParamsInDB
			)

			return DB.Parameter.insertMany(getParamsToInsert())
		})
}

/**
 * Request parameters from database, and put them in params
 */
function requestParameters() {
	return DB.Parameter.find({ name: { $in: R.keys(paramsList) }}).then(dbParameters => {
		const format = docs => {
			const getAttr = attr => R.map(R.prop(attr))(docs);

			return R.zipObj(getAttr('name'), getAttr('value'))
		}

		return Object.assign(params, format(dbParameters))
	})
}

/**
 * Update a parameter in database AND in params
 * @param {string} paramName 
 * @param {*} value
 */
const setParameter = (paramName, value) => new Promise((resolve,reject) => {
	if(is.undefined(paramsList[paramName]))
		return reject(log.err(`Cannot set parameter ${paramName}, it does not exist`))
	
	return DB.Parameter
		.findOneAndUpdate({ name: paramName }, { value }, { new: true }, // Update in DB
		(err, dbParam) => { // Update in params with the value in the database
			params[paramName] = dbParam.value;
			resolve();
		})
})

/**
 * Update multiple parameters in database AND in params
 * @param {Object} parameters
 * @returns {Promise}
 */
function setParameters(parameters) {
	const promises = R.values(R.mapObjIndexed((paramValue, paramName) =>
		setParameter(paramName, paramValue), parameters)
	)

	return Promise.all(promises)
		.then(values => {
			// Update actionObjects and their arguments definitions
			// then send it to clients via socket
			require('../actions/actionObjects').init().then(() => {
				require('../socket').sendActions();
			});
			return values;
		})
		.catch(err => {
			return err;
		});
}

module.exports = {
	requestParameters,
	setParameters,
	createInDB,
	params,
	paramsList
}