/**
 * Default values of all parameters, also used to list all parameters
 */
module.exports = {
	X_SPEED: 100,
	Y_SPEED: 100,
	Z_SPEED: 100,
    A_SPEED: 100,

	X_ACC: 200,
	Y_ACC: 200,
	Z_ACC: 250,
    A_ACC: 200,

    X_INVERT: false,
    Y_INVERT: false,
    Z_INVERT: false,
    A_INVERT: false,

	X_CALIBRATION_SPEED: 100,
	Y_CALIBRATION_SPEED: 80,
	Z_CALIBRATION_SPEED: 80,
    A_CALIBRATION_SPEED: 80,

    SAFE_Z_POS: 250,

    HEAD_OFFSET_Z: 200,
    GROUND_OFFSET_Y: 300,
    TOOLBASE_OFFSET_X: 100,

    SEEDER_RETRACT_DURATION: 2,
    SEEDER_RETRACT_SPEED: 100,

    LOADCELL_ENGAGE_THRESHOLD: 20,

    SEEDER_OFFSET_Y: 50,
    PROBE_OFFSET_Y: 40,

    WATER_FLOW: 15,
    WATER_DURATION: 2,
    AIR_FLOW: 15,
    AIR_DURATION: 2,
}