'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const config = require('./config/environment');

process.env.PORT = config.port;

/**
 * Setting the global utils modules
 */
global.Promise = require('bluebird');
Promise.config({
	cancellation: true
})
global.mongoose = require('mongoose')
global.assert = require('assert')
global.log = require('./log')
global.Socket = require('./socket')
const expressServer = require('./express');
global.is = require('is_js')
global.R = require('ramda')
global._ = require('lodash')
global.DB = mongoose.models
global.P = require('./parameters').params;

log.info(`NODE ENVIRONMENT: ${process.env.NODE_ENV}`);
log.info('Start initialization...')

const connectToMongo = require('./mongo');
const createBot = require('./createBot').create;
const createParameters = require('./parameters').createInDB;
const parameters = require('./parameters').requestParameters;
const scheduler = require('./scheduler').init;
const hardware = require('./hardware').init;
const pi_hardware = require('./pi_hardware').init;
const socket = require('./socket').init;
const actionObjects = require('./actions/actionObjects').init;
const actionList = () => require('./actions/actions');
const express = expressServer.start;

const init = [
	connectToMongo,
	createBot,
	createParameters,
	parameters,
	hardware,
	pi_hardware,
	actionObjects,
	actionList,
	scheduler,
	socket,
	express
];

Promise.reduce(init, (_, task) => task(), null)
	.then(() => {
		log.info('Initialization end')
	})
	.catch(err => {
		throw err;
	})



	// var server = new(require('bluetooth-serial-port')).BluetoothSerialPortServer();

	// var CHANNEL = 10; // My service channel. Defaults to 1 if omitted.
	// var UUID = '38e851bc-7144-44b4-9cd8-80549c6f2912'; // My own service UUID. Defaults to '1101' if omitted
	
	// server.listen(function (clientAddress) {
	// 	console.log('Client: ' + clientAddress + ' connected!');
	// 	server.on('data', function(buffer) {
	// 		console.log('Received data from client: ' + buffer);
	
	// 		// ...
	
	// 		console.log('Sending data to the client');
	// 		server.write(new Buffer('...'), function (err, bytesWritten) {
	// 			if (err) {
	// 				console.log('Error!');
	// 			} else {
	// 				console.log('Send ' + bytesWritten + ' to the client!');
	// 			}
	// 		});
	// 	});
	// }, function(error){
	// 	console.error("Something wrong happened!:" + error);
	// }, {uuid: UUID, channel: CHANNEL} );


/**
 * Bluebird promise handling rejections 
 */

 const unhandledPromises = []

process.on("unhandledRejection", function(reason, promise) {
    // See Promise.onPossiblyUnhandledRejection for parameter documentation

	unhandledPromises.push(promise)

	log.err(reason)

	throw reason
});

process.on("rejectionHandled", function(promise) {
    // See Promise.onUnhandledRejectionHandled for parameter documentation

	log.err("dddd")

});

exports = module.exports = expressServer.app;