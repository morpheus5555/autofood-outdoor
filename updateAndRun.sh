git fetch gitlab prod
git reset --hard gitlab/prod

export NODE_ENV=production
/usr/bin/node --inspect=0.0.0.0:9229 server/app.js
