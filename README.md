# Autofood Bot-Node

Autofood Bot on Raspberry PI Side

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:9000
grunt server

# build for production, when package project must be run after FE build
grunt build

#run unit tests
grunt test
```

# Architecture

## Classes

<div style="text-align: center">
    <img src="./arch.svg"/>
</div>

## Activity

<div style="text-align: center">
    <img src="./activity.svg"/>
</div>


License

MIT © autofood