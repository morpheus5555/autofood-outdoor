process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const chai = require('chai')
const expect = chai.expect;
global.is = require('is_js')
global.assert = require('assert')
global.log = require('../server/log')
global.mongoose = require('mongoose')

const aO = require('../server/actions/actionObjects')

//require('../server/mongo').
aO.init().then(() =>

describe('ActionObject', function() {
	describe('#checkArgument()', function() {
		it('should return an error when value is undefined and doesn\'t have a default', function() {
			expect(aO.checkArgument('axis', undefined)).to.be.an('error');
		});
		it('should return an error when value isn\'t of the right type', function() {
			expect(aO.checkArgument('axis', 2)).to.be.an('error');
		});
		it('should return an error when value isn\'t in select', function() {
			expect(aO.checkArgument('axis', 'hey')).to.be.an('error');
		});
		it('should return an error when value is < min', function() {
			expect(aO.checkArgument('delay', -50)).to.be.an('error');
		});
		it('should return an error when value is > max', function() {
			expect(aO.checkArgument('acc', Number.MAX_SAFE_INTEGER)).to.be.an('error');
		});

		it('should return default value when undefined', function() {
			expect(aO.checkArgument('speed')).to.equal(aO.definition.arguments.speed.default);
		});
		it('should return value when valid', function() {
			expect(aO.checkArgument('axis', 'x')).to.equal('x');
			expect(aO.checkArgument('acc', 100)).to.equal(100);
			expect(aO.checkArgument('delay', 0)).to.equal(0);
		});
	});
})

)