process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var assert = require('assert');
var log = require('../server/log');

//
var mongoose = require('mongoose');
var config = require('./../server/config/environment');

mongoose.connect(config.db.uri, config.db.options);
mongoose.connection.on('error', log.info);
//

var getBot = require('../server/getBot');

describe('Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {
      assert.equal([1,2,3].indexOf(4), -1);
    });
  });
});

describe('Bot', function() {
    describe('Exist #getBot()', function() {
      it('should have a bot in the database', function(done) {
            getBot(function(bot) {
                if(!bot || bot === null) done('No bot found !');
                else done();
            });
      });
    });
  });

  describe('Axis', function() {
    it('should have 4 axis', function(done) {
        getBot(function(bot) { // crap cpy
            if(!bot || bot === null) done('No bot found !')
            else {
                 var axes = Object.keys(bot.axis);
                if(!axes) done('Bot has no Axis !')
                else if(axes.length-1 === 4) done();
                else done(`Bot has ${axes.length-1} Axis !`);
            }
        });
    });
  });

  describe('Ground', function() {

    describe('Foods', function() {
      const n_axis = [{},{},{}];
      it('can create a food', function() {
          assert.equal([1,2,3].indexOf(4), -1);
      });
    });
  });

  describe('ActionQueue', function() {
    describe('Actions', function() {

    });
    describe('Events', function() {

      });
  });